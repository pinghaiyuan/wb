typedef struct Node {
struct Node *children;
struct Node *parent;
struct Node *next;
struct Node *lastChild;
struct Node *attributes;
char *name;
char *value;
char *type;
char *extra;
int refcount;
} Node;


