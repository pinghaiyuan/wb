from distutils.core import setup, Extension

module1 = Extension('_hubbub_parser',
extra_compile_args=["-I","../../libs/include"],
extra_link_args=["-L","../../libs/lib","-Wl,-R$ORIGIN/../libs/lib/"],
libraries=['hubbub','parserutils'],
                    sources = ['hubbub_parser.c']
)

setup (name = 'wb.parser_hubbub_parser',
       version = '1.0',
       description = 'html parser interface to hubbub',
       ext_modules = [module1])

