#gcc -O3 -Wall -fPIC -shared -static-libgcc -Wl,-static -oparser_new.so parser_newnosharedmem.c -lhubbub -lparserutils 2>&1 | more
import utils
utils.log("logging from parser_new (CTypes connector to libhubbub)")
import time,sys,_hubbub_parser

def parse(s):
 x=_hubbub_parser.pyParseString(s)
 fh=utils.open("logs/parse.%s.txt" % (str(time.time()),),"wb")
 fh.write(str(x))
 fh.flush()
 fh.close()
 return x

if __name__=='__main__':
 import sys
 sys.stderr=sys.stdout
 print "running"
 x=int(sys.argv[2]) if len(sys.argv)>2 else 1
 for i in xrange(x):
  t=time.time()
  x=parse(open(sys.argv[1],"rb").read())
  print time.time()-t
