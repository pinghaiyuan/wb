from utils import log,generate_error_report
import threading,sys,utils,os
log("logging from downloads")

class singleDownload(threading.Thread):
 def __init__(self,out_q=None,url=None,method="get",cookies=None,headers={"user-agent":"Mozilla/5.0 (Linux 3.2; rv:15.0) Gecko/20100101 Firefox/15.0.1; wb/1.0"},filename=None,offset=0,verify=None):
  threading.Thread.__init__(self)
  self.daemon=1
  self.verify=verify
  self.cookies=cookies
  self.out_q=out_q
  self.url=url
  self.headers=headers
  self.method=method
  self.filename=filename if filename!=None else utils.filename("downloads/"+url.split("?",1)[0].rstrip("/").rsplit("/",1)[-1])
  self.offset=offset
  self.getGoodFilename()

 def getGoodFilename(self):
  if os.path.exists(self.filename):
   filename,ext=self.filename.rsplit(".",1)
   if "/" in ext:
    filename,ext=self.filename,""
   x=filename[filename.rfind("("):]
   if "(" in x and ")" in x and x[1:-1].isdigit():
    x="("+str(int(x[1:-1])+1)+")"
    filename=filename[:filename.rfind("(")]+x
   else:
    filename=filename+"(1)"
   self.filename=filename+('' if not ext else '.'+ext)
   if os.path.exists(self.filename):
    self.getGoodFilename()

 def run(self):
  buff=4096
  log("download:run",self.url,self.filename,str(self.cookies),str(self.headers))
  try:
   import requests
   req=getattr(requests,self.method)(url=self.url,cookies=self.cookies,headers=self.headers,verify=self.verify)
  except Exception,e:
   log("download:error"); generate_error_report()
   sys.exit()
  s=req.status_code
  if not (s>=200 and s<400):
   self.out_q.put(dict(name="download.cancel",url=self.url,filename=self.filename,reason="Download URL returned error %d (continuing to download anyway)" % (s,),code=s))
  l=float(req.headers.get('content-length',0))
  self.out_q.put(dict(name="download.status",url=self.url,filename=self.filename,progress=(-1,l)))
  counter=0
  t=None
  reader=req.iter_content(buff)
  try:
   fh=open(self.filename,"wb")
  except Exception,e:
   self.out_q.put(dict(name="download.cancel",url=self.url,filename=self.filename,reason=e.args[-1],code=e.args[0]))
   sys.exit()
  for t in reader:
   if t=='':
    break
   counter+=len(t)
   self.out_q.put(dict(name="download.status",url=self.url,filename=self.filename,progress=(counter,l)))
   fh.write(t)
#   if counter>=l and l!=0:
#    break
  fh.close()
  self.out_q.put(dict(name="download.complete",url=self.url,filename=self.filename,progress=(counter,counter)))
  sys.exit()

