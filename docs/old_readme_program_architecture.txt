queue's are basically pipes, and the terminology is used interchangeably in this file.
The queues are two-way pipes.
Only one event can be pulled from the pipe at a time, and if nothing is available, then the pipes will return None.
Only one event can be pushed to the pipe at a time, though an event can be any python object, from a list, to another q, to a dict.

create a set of queues.
create a window (this is where the user will control things).
This is the ActiveWindow class.
inside ActiveWindow:
user loads a page (the homepage for example)
the pipe labeled out_q is pushed into the webpage class as in_q.
the pipe labeled in_q is pushed into the webpage class, and is attached with the name out_q
anything put into the out_q of the activeWindow class will be sent to
the webpage class, and anything in the out_q of the webpage class will
be sent back to ActiveWindow.
Inside the ActiveWindow class, the while loop inside the run method continuously checks for new events coming down its in_q pipe, and for key presses.

Important!
This is the pattern for all threads in wb; each thread checks its in_q, or inbox, for events.
If a function/method named in the receiving class exists, then said function is run, with the full event from the pipe as its argument.
If a method does not exist, and the method is not a specialized one, then the event is dropped.
If the method does not exist in the class, but does exist as a special
event (set and get, for example), then an action will be taken on the
event, depending on the type of event.
In some cases, the event might be pushed directly to self.out_q.

page class:
inside the page class, the specified webpage is grabbed. (If none is given, the homepage is grabbed).
The desired url is sent, along with one end of in-q, from the page class to the fetcher q.
the fetcher grabs the page, and sends it back through the q that it received with the url.
Basically, we sent one end of a hose through the fetcher's hose, and
attached the url to it. then the fetcher grabbed the webpage, shoved
it back down the inside hose.

The page class waits for the event to come down it's in_q hose with the name "data".
Then, it passes the url and webpage to its data function.
in_q and out_q are still connected to the ActiveWindow class.

in the data function:
if the webpage dictionary has an attribute of x-error, the data function sends an error message through out_q.
This message arrives on in_q inside the ActiveWindow.
It's name is url.error, which is changed to urlError, and the urlError function is called inside ActiveWindow class.
in the urlError method, the status bar is set to "bad, url", where url is the url that gave an error in question.

back in the data function:
On the other hand, if the url arrives correctly and without error, the
url is pushed back down self.out-q, from page class to ActiveWindow,
and urlSuccess is called. This displays the "loaded url" message.

back in the data function:
If the url is a success:
The body of the webpage is grabbed. (If the page was not html, this would be where we'd notify the ActiveWindow that we had a weird file).
This content is saved to self.content, (e.g. in the page classes personal memory).
Then the page.loadComplete name is put into self.in_q, which basically tells the page class to run it. We're sending loadComplete back to its same class.

inside pageLoadComplete:
We pull down all the external resources from the document (javascript, css (if desired), images (if desired), etc).
This is accomplished in resolveToFullDocument.

in resolveToFullDocument:
we turn the document from a big string into an lxml document.
We itterate through the document, finding all those
script/img/whatever elements with src attributes. We resolve them into
full links, remove that specific attribute, then grab the content from
the specified url, and put it inside the element we just accessed.
e.g. we find a script element. we grab the src attribute, resolve it
from /js/jquery.js to http://site.com/js/jquery.js. We delete the src
attribute from the script element. inside the parent of the script
element, we delete the script element. we replace it with a plain
script element that we create, whose text is that from
http://www.site.com/js/jquery.js.
We turn the xml object back into a big string, and return it.

back in pageLoadComplete:
We make a full dom object from the results of the fully resolved webpage.
we log the fully resolved dom object.
We send the pageRefresh message (along with the dom object, and the screen object (which is the webpage turned into a serial list)), into out_q. remember that out_q is connected to the ActiveWindow's in_q.
ActiveWindow receives the pageRefreshData message.

In pageRefreshData:
The screen and dom objects are saved, and screenModelToText runs.

in screenModelToText:
This turns the serial version of the webpage from:
start html, start head, start title attrs=lang=en, text=document title, end title, end head, start body, end body, end html
to:
[[ic=4,"document title"]]
All text is wrapped to fit the current screen size.
the text is painted to the screen with textObjectToVisibleScreen.

back in pageLoadComplete:
While all the converting and painting is going on, the pageLoadComplete function is working on the page scripting.
It creates two pipes, self.in_q_js and self.out_q_js.
It then starts the javascript thread, setting the threads in-q to self.out_q_js, and the threads out_q to self.in_q_js.
All the scripts are grabbed from the webpage, and send down the self.out_q_js pipe, to land in the JSThread's in_q.

In the JSThread:
The PyV8 interpreter is thread-locked, (so nothing else interfears with it), and initialized with the dom object from the page class.
Then, it waits for events to come to it, like the scripts from above.
As each script hits the in_q of JSThread, the PyV8 context is locked, and the piece of code is run.
If code has to pass back out of the PyV8 context, (e.g. a piece of javascript sets the statusbar), then said item is written to out_q of the JSThread.
This q, self.in_q_js, is read, along with the page classes in_q, by the page class, in its while loop.
If appropriate, the event pushed back from the javascript class is
sent to the window class. (e.g. a message to set the statusbar is
going to need to be sent to ActiveWindow).

