#DOM and parser for HTML and Javascript objects, ajax handler, part of events support (the window class is the global class for js)
import utils,urlparse,PyV8,re,string,random,time,hashlib,sys,urllib,os,Queue,traceback,collections,threading,binascii
#import inspect
import sys
if __name__=='__main__':
 sys.stderr=sys.stdout
import urlFetcher
import StringIO as stringio
from events import *
from utils import makeFullLink,context,zeroDict,log
try:
 import unidecode
except:
 pass
log("logging from newDom")
try:
 os.remove(os.path.expanduser("~/.wb/logs/scriptLog.txt"))
except:
 pass
parser="html5lib"
if parser=="html5lib":
 import html5lib
else:
 import hubbub_parser

def generate_error_report(e=None):
 fp = stringio.StringIO()
 traceback.print_exc(file=fp)
 message = fp.getvalue()
 del fp
 log(message)
 return message

formElements=set("input,button,textarea,select,option".upper().split(","))

class urlDecomposition(object):
 """
provides urlDecomposition and setter functions for elements supporting urlDecomposition
requires:
string getUrl (void)
void setUrl(string)
"""
 urlDecompIgnoreInit=0
 def urlDecompInit(self):
  if self.urlDecompIgnoreInit:
   return
  p=urlparse.urlparse(self.getUrl())
  self.urlDecompAttrs=utils.box()
  for i in "scheme,username,password,port,hostname,fragment,query,path".split(","):
   setattr(self.urlDecompAttrs,i,getattr(p,i))
 @property
 def protocol(self):
  self.urlDecompInit()
  return self.urlDecompAttrs.scheme+":"
 @protocol.setter
 def protocol(self,x):
  self.urlDecompAttrs.scheme=x.rstrip(":")
  self.decompUpdate()
 @property
 def hostname(self):
  self.urlDecompInit()
  return self.urlDecompAttrs.hostname
 @hostname.setter
 def hostname(self,x):
  self.urlDecompAttrs.hostname=x
  self.decompUpdate()
 @property
 def hash(self):
  self.urlDecompInit()
  return "#"+self.urlDecompAttrs.fragment if self.urlDecompAttrs.fragment else ''
 @hash.setter
 def hash(self,x):
  self.urlDecompAttrs.fragment=x[1:] if x.startswith("#") else x
  self.decompUpdate()
 @property
 def host(self):
  self.urlDecompInit()
  if self.port and self.port.isdigit() and int(self.port)!=self.protocols.get(self.protocol,"http"):
   return self.hostname+":"+self.port
  else:
   return self.hostname
 @host.setter
 def host(self,x):
  if ":" in x:
   self.hostname,self.port=x.split(":",1)
  else:
   self.hostname=x
  self.decompUpdate()
 @property
 def port(self):
  self.urlDecompInit()
  return self.urlDecompAttrs.port if self.urlDecompAttrs.port else ''
 @port.setter
 def port(self,x):
  self.urlDecompAttrs.port=int(x) if x.isdigit() and 0<int(x)<65536 else self.port
  self.decompUpdate()
 @property
 def pathname(self):
  self.urlDecompInit()
  return self.urlDecompAttrs.path
 @pathname.setter
 def pathname(self,x):
  self.urlDecompAttrs.path=x
  self.decompUpdate()
 @property
 def search(self):
  self.urlDecompInit()
  return "?"+self.urlDecompAttrs.query if self.urlDecompAttrs.query else ''
 @search.setter
 def search(self,x):
  self.urlDecompAttrs.query=x.lstrip("?")
  self.decompUpdate()
 def decompUpdate(self):
  self.urlDecompIgnoreInit=1
  h=[]
  if self.protocol:
   h.append(self.protocol)
  else:
   h.append("http:")
  h.append("//")
  if self.hostname:
   h.append(self.hostname)
  if self.port:
   h.append(":"+str(self.port))
  if self.pathname:
   h.append(self.pathname)
  else:
   h.append("/")
  if self.search:
   h.append(self.search)
  if self.hash:
   h.append(self.hash)
  ret="".join(h)
  self.setUrl(ret)
  self.urlDecompIgnoreInit=0

class Parser(object):
 @property
 def cur(self):
  return self._cur
 @cur.setter
 def cur(self,x):
  self.doc._current=x
  self._cur=x
  return x
 def __init__(self,url=None,resolver=None,parent=None,fragment=0,pageQ=None):
  self.key=url if url else "nowurl"+str(time.time())
  self.url=url
  self.urlsToNodes={}
  self.resolver=resolver
  self.log=None
  self.fragment=fragment
  self.pageQ=pageQ
  if self.fragment==0:
   self.w=window(resolver=resolver,pageQ=pageQ,url=url,parent=parent,fragment=fragment)
   lck=PyV8.JSLocker() if not PyV8.JSLocker.locked else context(locked=1)
   log("locked:",lck.locked)
   with lck:
    ctx=PyV8.JSContext(self.w)
    log("assigning ctx to w")#; time.sleep(2)
    self.w.resolver=self.resolver
    self.w._jsctx=ctx
   with lck:
    with self.w._jsctx:
     self.w._jsctx.eval("""
//js includes for wb
var Iterator = function(arr){ return {
index : -1,
hasNext : function(){ return this.index < arr.length-1; },
hasPrevious: function(){ return this.index > 0; },
current: function(){ return arr[ this["index"] ]; },
next : function(){if(this.hasNext()){this.index = this.index + 1;return this.current();}return false;},
previous : function(){if(this.hasPrevious()){this.index = this.index - 1;return this.current();};return false;}
}   
};
""")
#     self.w._jsctx.eval("window=this")
   self.doc=document(window=self.w)
   self.doc._parser=self
   self.doc.ownerDocument=self.doc
   self.w.setDocument(self.doc)
#   self.w.ownerDocument=self.doc
   self.state=0
   self._html=self.doc.appendChild(HTMLHtmlElement("html"))
   self._head=self._html.appendChild(HTMLHeadElement("head"))
   self._body=self._html.appendChild(HTMLBodyElement("body"))
   self._elems=self.doc._TAGS
   self.cur=self.doc
  else:
   self.doc=document()
   self.doc.ownerDocument=self.doc
   self.state=-1
   self.cur=self.doc
   self._elems=self.cur._TAGS
  self.base=self.url
  if self.fragment==0:
   self.w.document._base=self.base
  else:
   self.doc._base=self.base


 def feed(self,fc):
  log("parsing:",len(fc))
  if parser=="html5lib":
   if self.fragment==0:
    x=html5lib.parse(fc)
   else:
    x=html5lib.parseFragment(fc)
  else:
   if self.fragment==0:
    x=hubbub_parser.parse(fc)
   else:
    x=hubbub_parser.parse(fc)
  if self.fragment==0:
   lck=PyV8.JSLocker() if not PyV8.JSLocker.locked else context(locked=1)
   with lck:
    with self.w._jsctx:
     if parser=="html5lib":
      self.html5lib_doParse(x)
     else:
      self.doParse(x)
   del lck
  else:
   if parser=="html5lib":
    self.html5lib_doParse(x)
   else:
    self.doParse(x)
   if hasattr(self,"page"): self.page.setStatus("parse time: %.2f" % (time.time()-a,))

 def doParse(self,l):
  text=""
  tag=""
  attributes={}
  dtattrs=[]
  k=0
  i=0
  ll=len(l)
  html,head,body=0,0,0
  self_fragment=self.fragment
  full=[]
  while i<ll:
   j=[obj for obj in l[i]]
#   utils.log("progress:",j)
#set all values to strings if they are None, so everything will be a string
   [j.__setitem__(x,j[x] if j[x]!=None else '') for x in xrange(len(j))]
   fragment_tags=("html","head","body")
   if self.fragment==1 and j[0]=="start" and j[1] in fragment_tags and locals()[j[1]]==0:
    locals()[j[1]]=locals()[j[1]]+1; i+=1; continue
   elif self.fragment==1 and j[0]=="end" and j[1] in ("html","head","body") and locals()[j[1]]==0:
    locals()[j[1]]=locals()[j[1]]-1; i+=1; continue
   elif j[0]=="start":
    tag=j[1]
    if self_fragment==1 and tag in fragment_tags: locals()[tag]+=1
    k=i+1
    if tag=="": i+=1; continue
    while l[k][0]=="attribute" and k<ll:
     attributes[l[k][1]]=l[k][2]
     k+=1
    i=k
    full.append(("start",tag,attributes))
#    self.start(tag,attributes,_flags="nofetch")
    tag=""
    attributes={}
    continue
   elif j[0]=="comment":
    full.append(("comment",j[2]))
#    self.comment(j[2])
    i+=1
    continue
   elif j[0]=="end":
    if self_fragment==1 and j[1] in fragment_tags: locals()[tag]-=1
    full.append(("end",j[1]))
#    self.end(j[1])
    i+=1
    continue
   elif j[0]=="text":
    text+=j[2]
    k=i+1
    while k<ll and l[k][0]=="text":
     text+=l[k][2]
     k+=1
    i=k
    if text!="":
     full.append(("text",text))
#self.data(text)
    text=""
    continue
   elif j[0]=="doctype":
    dtattrs=[j[1],j[2],j[3]]
    dtattrs.insert(0,"doctype")
    full.append(dtattrs)
    dtattrs=[]
    i+=1
    continue
   elif j[0] =="#root":
    i+=1
    continue
   else:
    utils.log("unknown:",i,j)
    i+=1
    continue
  base=[i for i in full if i[0]=="start" and i[1]=="base" and "href" in i[2]]
  if base:
   self.doc._base=base[2]['href']
  else:
   self.doc._base=self.url
  zz='''
  srcs=[makeFullLink(i[2]['src'],self.doc._base) for i in full if i[0]=="start" and i[1] in ("script","style") and "src" in i[2]]
  if srcs:
   [urlFetcher.addUrl(i,dataOnly=1,key=self.key) for i in srcs]
   if hasattr(self,"page"): self.page.setStatus("Fetching %d external resources" % (len(srcs),))
   res=urlFetcher.getAllUrls(key=self.key)
   if hasattr(self,"page"): self.page.setStatus("Fetched %d/%d external resources" % (len(res),len(srcs),))
'''
  for i in full:
   getattr(self,i[0])(*i[1:])

#~~

 def html5lib_doParse(self,i):
#  log("doParse",i.name,i.type)
  if i.value==None: i.value=''
  if i.name==None: i.name=''
  if 1==0 and i.name in ["embed","noembed","noframes"] and i.childNodes:
   a=i.childNodes[0]
   b=parseString(a.value,url="#",fragment=1)
   i.removeChild(a)
   [i.appendChild(j) for j in b]
  if i.type==3:
   self.doctype(i.name,i.publicId,i.systemId)
#   for j in i.childNodes:
#    self.doParse(j)
  elif i.type==4:
   self.data(i.value)
  elif i.type==5:
   self.start(i.name,i.attributes)
   for j in i.childNodes:
    self.html5lib_doParse(j)
   self.end(i.name)
  elif i.type==6:
   self.comment(i.data)
  else:
   for c in i.childNodes:
    self.html5lib_doParse(c)

 def addNode(self,n):
#  log("adding node "+str(repr(n))+" self.state="+str(self.state))
  d=None
  ret=None
  old_state=None
  if self.state!=-1 and n.tagName not in ["BODY","HTML","HEAD"]:
   old_state=self.state
   self.state=-1
  if self.state==-1:
   ret=self.cur.appendChild(n)
  if (n.tagName=="HTML" and self.state==0) or (n.tagName=="HEAD" and self.state==1) or (n.tagName=="BODY" and self.state==2):
   d=getattr(self,"_"+n.tagName.lower())
   for k,v in n.attrs.items():
    d.attrs[k]=v
   d.updateActiveAttrs()
   for i in n.childNodes:
    d.appendChild(i)
  if self.state in [0,1,2] and d==None:
   ref=[self._html,self._head,self._body][self.state]
   ret=ref.parentNode.insertBefore(n,ref)
  if self.state in [3,4] and d==None:
   ref=[self._body,self._html][self.state-3] if d==None else d
   ret=ref.appendChild(n)
  if old_state!=None:
   self.state=old_state
  if d!=None and self.state<2:
   self.state+=1
  return ret if d==None else d

 def pi(self,x):
  x=stripWeird(x,0)
  #open("log","a").write("pi\n")
  self.addNode(self.doc.domimpl.createProcessingInstruction(*x.split(" ",1)))
 def doctype(self,name,pubId,systemId):
  name,pubId,systemId=stripWeird(name,0),stripWeird(pubId,0),stripWeird(systemId,0)
  #open("log","a").write("doctype\n")
  self.addNode(self.doc.domimpl.createDocumentType(name,pubId,systemId))
 def comment(self,x):
  x=stripWeird(x,0)
  #open("log","a").write("comment\n")
  self.addNode(Comment(x))
 def addUrlToDom(self,url,destNode,dataOnly=1):
  urlFetcher.addUrl(url,dataOnly=dataOnly,key=self.key)
  if url not in self.urlsToNodes: self.urlsToNodes[url]=[]
  self.urlsToNodes[url].append(destNode)
 def start(self,tag,attrs,_flags=""):
  tag=tag.split("}",1)[-1].lower()
  tag=stripWeird(tag,0)
  attrs=dict([(stripWeird(k.rsplit("}",1)[-1],0),stripWeird(str(v) if not isinstance(v,basestring) else v,0)) for k,v in attrs.items()])
  if tag=="base" and "href" in attrs:
   self.base=attrs['href']
   self.doc._base=self.base
   log("base found",self.base)
  #open("log","a").write("start\n")
  #open("log","a").write("parser.start"+str(tag)+"\n")
  nelem=self._elems[tag] if tag in self._elems else HTMLElement
  nelem=nelem(tag)
  nelem.attrs.update(attrs.items())
#  if _flags: [setattr(nelem._flags,i,1) for i in _flags.split(",")]
  self.cur=self.addNode(nelem)
  if self.fragment==1:
   self.cur._flags.is_parser_inserted=1
   self.cur._flags.is_fragment=1
#  if self.resolver!=None and "src" in self.cur.attributes and self.cur.getAttribute("src").strip()!="" and self.fragment==0:
#   if self.cur.nodeName in ("style","script"):
#    url=makeFullLink(attrs['src'],self.doc._base)
#    if hasattr(self,"page"): self.page.setStatus("..."+"/".join(url.rsplit("/",3)[1:]))
#    t1=time.time()
#    self.cur._flags.hasRun=0
#    self.addUrlToDom(url,self.cur)
#    txt=self.resolver(url,dataOnly=1)
#    t2=time.time()
#    if hasattr(self,"page"): self.page.setStatus("loaded in %s." % (str(t2-t1),))
#    self.cur.appendChild(Text(stripWeird(txt,1)))
   if 1==0 and (self.cur.nodeName in ("FRAME","IFRAME")) and self.fragment==0:
#    return
    url=self.cur.getAttribute("src")
    url=makeFullLink(url,self.doc._base)
    p=Parser(url=url,resolver=self.resolver,parent=self.cur,pageQ=self.pageQ)
    p.feed(self.resolver(url,dataOnly=1))
    ret=p.close()
    ret=ret._jsctx.eval("window")
    x=self.addNode(ret)
    self.w.frames[len(self.w.frames)]=x

#~~
 def text(self,*x):
  return self.data(*x)

 def data(self,x):
  #open("log","a").write("data\n")
  d=x
#  if "<" in d: 
  stripParm=0
  if self.cur.nodeName=="SCRIPT":
#   sh=open("./scriptLog.txt","a"); sh.write(d+"\n$$$\n"); sh.close()
   stripParm=1
  d=stripWeird(d,stripParm)
  nt=Text(d)
  self.addNode(nt)

 def end(self,tag):
  #open("log","a").write("end\n")
  tag=tag.split("}",1)[-1]
#  log("end::",tag,self.cur,self.cur._flags)
  if tag=="script" and self.fragment==0 and not self.cur._flags.hasRun and self.cur.getAttribute("type") in ("text/javascript",None):
#   self.page.setStatus("eval start")
   if hasattr(self,"page"): self.page.setStatus("running")
#   t1=time.time()
#   log("running script",str(self.cur))
   self.cur.eval()
   if "load" in self.cur.events: self.cur.evt_load()
#   if hasattr(self,"page"): self.page.setStatus("time %s " % (str(time.time()-t1),))
#   self.cur._flags.hasRun=1
#   if hasattr(self,"page"): self.page.setStatus("running finished")
#   self.page.setStatus("eval done")
  self.cur=self.cur.parentNode
  if self.state!=-1 and [self._html,self._head,self._body] in self.cur.childNodes and self.state<4:
   self.state+=1

 def close(self):
  page=hasattr(self,"page")
  if page: self.page.setStatus("added all URL's. Now requesting contents.")
#  urls=urlFetcher.getAllUrls(self.key)
#  lurls=len(urls)
#  for url in urls:
#   if page: self.page.setStatus("appending url %d/%d" % (urls.index(url),lurls,))
#   for j in self.urlsToNodes.get(url.url,[]):
#    j._flags._gotsource=1
#    log("appending",url.content,"to node",str(j))
#    j._flags.hasRun=0
#    j.appendChild(Text(url.content))
#    j.eval()
#    with PyV8.JSLocker() as lck:
#     j.parentNode._takeOwnership(j)
#  if page: self.page.setStatus("added all")
#[[i.appendChild(url.content) for i in j] for j in self.urlsToNodes[url.url]]
#  log("closing parser. fragment=",self.fragment,"lfc=",self.lfc)
  if self.fragment==1:
   if parser=="html5lib":
    return self.doc.childNodes
   return self.doc.childNodes
   b=[]
   a=self.doc.childNodes
   [[b.append(i) for i in j.childNodes] for j in a]
   return b
  elif self.fragment==2:
   return self.doc
  else:
   try:
    lck=PyV8.JSLocker() if not PyV8.JSLocker.locked else context(locked=1)
    with lck:
#     log("lck:",lck,lck.locked)
     with self.w._jsctx:
#      log("lck:",lck,lck.locked)
 #     log("body:",type(self.w.document))
#    if 1==1:
#     if 1==1:
      try:
       self.w.document.readyState="interactive"
      except Exception,e:
       log('error in `load function`',str(e),str([repr(i) for i in self.w.document._all])); generate_error_report()
   except Exception,e:
    log("strangeError:",e)
   return self.w

class parserThread(threading.Thread):
 def __init__(self,fc="",callback_q=None,pageQ=None,url="",resolver=None,parent=None,fragment=0,returnDocument=0):
  threading.Thread.__init__(self)
  self.daemon=1
  self.callback_q=callback_q
  self.fc=fc
  self.url=url
  self.resolver=resolver
  self.parent=parent
  self.pageQ=pageQ
  self.fragment=fragment
  self.returnDocument=returnDocument

 def parseString(self,fc,url="",resolver=None,parent=None,pageQ=None,fragment=0,returnDocument=0):
  p=Parser(url=url,resolver=resolver,parent=parent,pageQ=pageQ,fragment=fragment)
  p.feed(fc)
  if returnDocument:
   p.fragment=2
  x=p.close()
  return x

 def run(self):
  x=self.parseString(self.fc,url=self.url,resolver=self.resolver,parent=self.parent,pageQ=self.pageQ,fragment=self.fragment)
  self.callback_q.put(dict(name="page.new.generated.dom",dom=x))
  return

def parseString(fc,url="",resolver=None,parent=None,pageQ=None,fragment=0,returnDocument=0):
 p=Parser(url=url,resolver=resolver,parent=parent,pageQ=pageQ,fragment=fragment)
 p.feed(fc)
 if returnDocument:
  p.fragment=2
 x=p.close()
 return x

def prop(name, attrtype=str, readonly=False, default=''):
# default=''
 def getter(self):
  return attrtype(self.attrs[name]) if name in self.attrs else default
 def setter(self, value):
  self.attrs[name]=attrtype(value)
 return property(getter) if readonly else property(getter, setter)

class BaseClass(PyV8.JSClass):
# def __getattr__(self,x):
#  return self.__dict__.get(x.lower(),None)
# def __setattr__(self,x,y):
#  self.__dict__[x.lower()]=y
 _isNode=0
 _isElement=0
 events={}

 """
 def keys(self):
  return [i for i in dir(self) if not i.startswith("__") and not i.endswith("__")]
 def values(self):
  return [self[k] for k in self.keys()]
 def items(self):
  return zip(self.keys(),self.values())
 def has_key(self,key):
  if i in set(self.keys()):
   return 1
  return 0
"""

 def _parseToNodes(self,x):
#  xml=lxml.html.tostring(lxml.html.fromstring("<x>"+x+"</x>"),method="xml")[3:-4]
  res=parseString(x,fragment=1,url=self.ownerWindow.document._base)
#  log("res::",str(res),"dom::",str(res))
  return res

 def __setitem__(self,x,y):
#  log(str(repr(self)).split(" at ")[0],":setitem:",str(x),str(y))
  if not x.isdigit():
   return setattr(self,x,y)
  self.frames[x]=y
  return self.frames[x]

 def __getitem__(self,x):
  if not x.isdigit():
#   log(str(repr(self)).split(" at ")[0]+":getitem:",str(x))
   try:
    return getattr(self,x)
   except Exception,e:
    log(str(repr(self)).split(" at ")[0]+":getitem:",str(x))
    raise e
#   try:
#   except:
#    return None
  return self.frames[x] if x < len(self.frames) else None

 classLookup={}
 id=0

 def backendNoAsync(self,name,value="",**kw):
  a=time.time()
  q=Queue.Queue()
  res=[(k,v) for k,v in kw.items() if type(v) in [PyV8.JSFunction,PyV8._PyV8.JSFunction]]
  for k,v in res:
   self.classLookup[id(v)]=v
   kw[k]=id(v)
#  self.pageQ.put(dict(name=name,value=value,**kw))
  rc=self.generateRandomCode()
  self.pageQ.put(dict(callback_q=q,name=name,value=value,**kw))
#  log("backend listening:",q,name,value,kw)
#  ret=q.get()
#  log("backendRet:",ret)
#  return ret
  t=time.time()
  while t+0.2>time.time():
   try:
    ret=q.get(block=0)
    break
   except Queue.Empty:
    ret=None
#   if ret!=None:
  log("backendNoAsync","result:",ret,"Time",time.time()-a)
  try:
   return ret['value']
  except:
   return None
  return ''

 def backend(self,name,value="",**kw):
  log("backend:","name",name,"value",value,"kw",str(kw))
  res=[(k,v) for k,v in kw.items() if type(v) in [PyV8.JSFunction,PyV8._PyV8.JSFunction]]
  for k,v in res:
   self.classLookup[id(v)]=v
   kw[k]=id(v)
  self.pageQ.put(dict(name=name,value=value,**kw))

 def run(self,code,*args,**kw):
  self.ownerWindow.document._current=self.ownerWindow.document.body.lastChild
  if code in self.classLookup:
   e=self.classLookup[code]
   log("running:code:",code,str(e))
   try:
    return e(*args,**kw)
   except Exception,e:
    generate_error_report()
  elif not str(code).isdigit():
   try:
    self.ownerWindow._jsctx.eval(code)
   except Exception,e:
    log("tried running ",code,"as string with eval.\nerror:",str(e))
  else:
   log("invalid code id given to the dom.run function; the code linked to this id has not been found in the js code store",code)

 randNum=[48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122]
 randNumLen=len(randNum)-1
 def generateRandomCode(self,length=10,letters=0):
  if letters==1:
   return "".join([chr(self.randNum[random.randint(0,self.randNumLen)]) for i in xrange(length)])
  self.id+=1
  return self.id

 def removeEventListener(self,name,s,captures=0):
  if not name: return
  name=name.lower()
  evs=self.events.get(name,[])
  evs=[i for i in evs if i.captures==captures]
  [self.events[name].remove(i) for i in evs if i.original==s]

 def addEventListener(self,name,s,capture=0,fromAttribute=0):
  if not name: return
  name=name.lower()
  log("ael:",str(name),str(repr(self)),type(s),str(s))
  if fromAttribute==1 and isinstance(s,basestring):
   if s.startswith("javascript:"): s=s[11:]
   code="py_func_"+str(time.time()).replace(".","")[-6:]
   ss="(function %s (event) { %s })" % (code,s,)
   try:
    c=self.ownerWindow._jsctx.eval(ss)
   except Exception,e:
    log("Error:AEL:",str(ss))
    return None
   log("newCode:",str(c))
  elif isinstance(s,basestring):
   code="py_func_"+str(time.time()).replace(".","")[-6:]
   if s.strip().lstrip("(").startswith("function"):
    ss="(function %s (event) { return %s(event) })" % (code,s,)
   else:
    ss="(function %s (event) { %s })" % (code,s,)
   try:
    c=self.ownerWindow._jsctx.eval(ss)
   except Exception,e:
    log("Error:AEL:",str(ss))
    return None
   log("newCode:",str(c))
  else:
   c=s
  e=eventListener(self,name,c,capture,fromAttribute,s)
  self.events[name]=self.events.get(name,[])
  self.events[name].append(e)
  return e

 def dispatchEvent(self,evt):
  evtf=getattr(self,"evt_"+evt.type)
  ret=evtf(evt)
  return ret

 def click(self):
  ev=Event("click")
  ev.preventDefault()
  return self.dispatchEvent(ev)

#~/
 def eval(self,s=""):
#~~~
  self.ownerDocumentReal._current=self
  if self.nodeName=="SCRIPT":
   s=self.textContent
  elif "script" in self.parentNodeNames:
   s=self.parentNodes(name="script",count=1).textContent
  else:
   pass #s="//else"
#  log("eval:",str(s),"$$$$")
  s=s.strip()
  if s=='':
   return
#  s+=';for (var ivar in this) { if (typeof window[ivar]=="undefined"){window[ivar]=this.ivar; }}'
  try:
   s=str(s.encode('ascii'))
  except:
   log("javascript is not plain ascii, running conversions.")
   for i in ("utf-8","latin-1"):
    try:
     s=str(s.decode(i))
     break
    except:
     try:
      s=str(unidecode.unidecode(s.decode(i)))
      break
     except:
      continue
#window.ivarT=ivar.toString(); if (window.ivarT!="ivarT"&&ivarT!="ivar"&&ivarT!="_py_window_vars") { _py_window_vars+=ivarT+","; window[ivarT]=this.ivar;} window.log("jsLog:"+_py_window_vars)}'
  sl=utils.open("logs/scriptLog.txt","ab")
  sl.write(s+"\n//$$$$\n")
#+str(",".join([i for i in dir(self.ownerDocument.window) if i not in dir(window)+dir(document)]))+"\n")
  lck=PyV8.JSLocker() if not self.ownerWindow._jsctx.entered else context()
  with lck:
   with self.ownerWindow._jsctx:
    try: 
     ret=self.ownerWindow._jsctx.eval(s)
     sl.write("//executed script with no errors. "+str(len(s))+" characters.\n")
    except UnicodeDecodeError,e:
     try:
      ret=self.ownerWindow._jsctx.eval(s.decode('utf-8'))
     except Exception,e:
      sl.write("//Error (inside decoded code:"+str(e)+"\n"); generate_error_report()
      sl.write("//{{{{\n"+"\n".join(["//"+i for i in s.split("\n")]).rstrip()+"\n//}}}}\n//$$$$$\n")
    except Exception,e:
     sl.write("//Error:"+str(e)+"\n"); generate_error_report()
     sl.write("//{{{{\n"+"\n".join(["//"+i for i in s.split("\n")]).rstrip()+"\n//}}}}\n//$$$$$\n")
  del lck
  if not self.ownerWindow._jsctx.entered:
   unlck=PyV8.JSUnlocker()
  sl.close()


class DOMException(Exception):
 def __getattribute__(self,x):
  log("domException.__getattribute__",str(x),str(self))
  return Exception.__getattribute__(self,x)
 INDEX_SIZE_ERR     = 1  # If index or size is negative, or greater than the allowed value
 DOMSTRING_SIZE_ERR    = 2  # If the specified range of text does not fit into a DOMString
 HIERARCHY_REQUEST_ERR    = 3  # If any node is inserted somewhere it doesn't belong
 WRONG_DOCUMENT_ERR    = 4  # If a node is used in a different document than the one that created it (that doesn't support it)
 INVALID_CHARACTER_ERR    = 5  # If an invalid or illegal character is specified, such as in a name. 
 NO_DATA_ALLOWED_ERR   = 6  # If data is specified for a node which does not support data
 NO_MODIFICATION_ALLOWED_ERR = 7  # If an attempt is made to modify an object where modifications are not allowed
 NOT_FOUND_ERR      = 8  # If an attempt is made to reference a node in a context where it does not exist
 NOT_SUPPORTED_ERR     = 9  # If the implementation does not support the type of object requested
 INUSE_ATTRIBUTE_ERR   = 10 # If an attempt is made to add an attribute that is already in use elsewhere 

class TextRectangle(BaseClass):
 def __init__(self,top=0,bottom=0,left=0,right=0,width=0,height=0):
  self._top=top
  self._bottom=bottom
  self._height=height
  self._width=width
  self._left=left
  self._right=right
 @property
 def top(self):
  return self._top
 @property
 def bottom(self):
  return self._bottom
 @property
 def width(self):
  return self._width
 @property
 def height(self):
  return self._height
 @property
 def left(self):
  return self._left
 @property
 def right(self):
  return self._right

class eventListener(BaseClass):
 def __init__(self,elem,name,code,captures,fromAttribute=0,original=None):
  self.elem=elem
  self.name=name
  self.code=code
  self.captures=captures
  self.fromAttribute=fromAttribute
  self.original=original
 def run(self,event):
  w=self.elem.ownerWindow
  w['___func']=self.code
  w['___this']=self.elem
  w['___event']=event
  try:
   x=w._jsctx.eval("window.___func.call(window.___this,window.___event)")
#"try{window.___func.handleEvent.apply(window.___this,window.___event);}catch(err){window.___this.apply(window.___func,window.___event)};")
  except Exception,e:
   utils.generate_error_report()
   log(":event:error:"+str(self.name)+" on element "+repr(self.elem)+". ignoring.")
   x=1
  return x

class eventableElements(events):
 def updateActiveAttrs(self):
#  try:
#   log("uaa:",repr(self))
#  except:
#   pass
  if not getattr(self,"ownerWindow",None):
   return -1
  for k,v in self.attrs.items():
   k=k.lower()
   if k in self.event_names and self.events_added.k!=v:
    log("activeAttrib",repr(self),k,repr(self.events_added.k),str(v))
    setattr(self,k,v)
    self.events_added.k=v

class History(BaseClass):
 def __init__(self,url="",previous=""):
  self.url=url
  self.previous=previous

 @property
 def length(self):
  return 1

class Screen(BaseClass):
 def __init__(self):
  self.availHeight=768
  self.availWidth=1024
  self.colorDepth=24
  self.height=1024
  self.pixelDepth=24
  self.width=self.availWidth

class Navigator(BaseClass):
 def __getitem__(self,x):
  if type(x)==int:
   return getattr(self,[i for i in dir(self) if i not in dir(BaseClass) and not i.startswith("_")][x])
  return getattr(self,x)
 def __len__(self):
  return len([i for i in dir(self) if i not in dir(BaseClass) and not i.startswith("_")])
 def __init__(self,name="Mozilla/5.0 (Linux 3.1; rv:15.0) Gecko/20100101 Firefox/15.0.1; wb/1.0"):
  self.appName,self.version=name.split("/",1)
  self.appVersion=name
  self.appCodeName=self.appName
  self.language="US"
  self.mimetypes=["text/html","text/javascript","text/plain","text/rss"]
  self.platform="linux"
#fix to make plugins its own class
  self.userAgent=name
 @property
 def plugins(self):
  return NodeList([])
 @property
 def mimeTypes(self):
  return NodeList([])
 def javaEnabled(self):
  return 0

 def preference(self,x,y=""):
  return None

 def taintEnabled(self):
  return 1

class Array(list):
 @property
 def length(self):
  return len(self)

class Location(urlDecomposition,BaseClass):
 @property
 def href(self):
  try:
   b=self.ownerDocument._base
  except:
   b=""
  return makeFullLink(self.x,b)
 @href.setter
 def href(self,x):
  x=makeFullLink(x,self.href)
  self.setUrl(x)

 def __init__(self,x,owner=None):
  self.x=x
  if owner:
   self.ownerWindow=owner

 def __str__(self):
  return self.href

 def assign(self,x):
  self.replace(x)

 def replace(self,x):
  self.backend("go",url=x)

 def reload(self,forceGet=0):
  self.backend("js.reload")

 def getUrl(self):
  return self.href
 def setUrl(self,x):
  old=self.href
  new=makeFullLink(x,old)
  self.x=new
  ln=Location(new)
  lo=Location(old)
  if ln.host!=lo.host or ln.pathname!=lo.pathname or ln.search!=lo.search or ln.protocol!=lo.protocol:
   self.replace(self.x)

class visibleConstruct(BaseClass):
 def __init__(self,visible=1):
  self.visible=visible
 def a__setattr__(self,x,y):
  if x.lower()!="visible":
   return None
  self.visible=y
  return y
 def a__getattr__(self,x):
  if x.lower()!="visible":
   return self.__dict__[x]
  return self.visible

class XMLHttpRequest(BaseClass):
 @property
 def onreadystatechange(self):
  return self._onReadyStateChange

 @onreadystatechange.setter
 def onreadystatechange(self,f):
  self._onReadyStateChange=f

 @property
 def readyState(self):
   return self._readyState

 @readyState.setter
 def readyState(self,i):
  self._readyState=i
  if self.onreadystatechange!=None:
   log("execXmlReadyStateChange:",str(self.onreadystatechange))
   self.onreadystatechange()

 def __init__(self,window=None):
  self.window=window
  self.headers={}
  self._onReadyStateChange=None
  self.username=None
  self.password=None

 @property
 def withCredentials(self):
  return (self.username==None and self.password==None)

 def open(self,rType,uri,async=False,username="",password=""):
  log("xmlhttprequest.open",rType,uri)
  self.rType,self.uri,self.async,self.username,self.password=rType,uri,async,username,password

 def setRequestHeader(self,key="X_INVALID",value="NULL"):
  self.headers[key]=value

 def send(self,data=""):
  url=makeFullLink(self.uri,self.window.document._base)
  self.url=url
  log("xmlhttprequest.url:",url,"resolver",self.window.resolver,"data:","no data for get" if self.rType.lower()!="post" else data)
  try:
#nocache=1
   res=self.window.resolver(method=self.rType,url=url,data=data,headers=self.headers)
   self.res=res
   self.status=res.status_code
   if self.status<200: log("xmlerror:",res.error)
   self.statusText=""
   self.responseText=res.content
   log("xmlrequest.content","url:",url,"status:",self.status,len(self.responseText))
   self.readyState=4
  except Exception,e:
   self.readyState=0; generate_error_report()
  return self

 @property
 def responseXML(self):
  if not hasattr(self,"doc"):
   self.doc=parseString(fc=self.responseText,url=self.url,fragment=1,returnDocument=1)
  return self.doc

 def getResponseHeader(self,k=""):
  return self.res.headers.get(k,None)

 def getAllResponseHeaders(self):
  return "\n".join([":".join(i) for i in self.res.headers.items()])

class Console(BaseClass):
 def log(self,*x):
  log("js.console.log:",[str(i) for i in x])

 def debug(self,*x):
  log("js.console.debug:",[str(i) for i in x])

 def __call__(self,*x):
  return self.log(*x)

class DOMStorage(BaseClass):
 def __init__(self,persistent=0):
  self.clear()
 def getItem(self,x):
  return self.attrs.get(x,None)
 def setItem(self,x,y):
  self.attrs[x]=y
  return y
 def removeItem(self,x):
  self.attrs.pop(x) if x in self.attrs else ''
 def key(self,x):
  return self.attrs.get(self.attrs.keys()[int(x) if x.isdigit() else ''],None) if len(self.attrs)>x else None
 def clear(self):
  self.attrs={}
 def __getitem__(self,x):
  x=str(x) if x else ''
  if x.isdigit():
   return self.key(x)
  else:
   return self.getItem(x)
 def __setitem__(self,x,y):
  return self.setItem(x)

class window(eventableElements,BaseClass):
 wbEventsRunning=0
 def eval(self,s):
#  log("window.eval:",str(s))
  with PyV8.JSLocker():
   with self._jsctx:
    x=self._jsctx.eval(s)
  return x

 @property
 def ownerWindow(self):
  return self._document.window

 @property
 def _all(self):
  return self.document._all

 def __str__(self):
  return str(self.document)

 def escape(self,s):
  return urllib.quote(s)

 def unescape(self,s):
  return urllib.unquote(s)

 def btoa(self,x):
  return binascii.b2a_base64(x)

 def atob(self,x):
  return binascii.a2b_base64(x)

 def getComputedStyle(self,element,psudoElement=None):
  return CSSStyleDeclaration("",element)

 @property
 def document(self):
  return self._document
 @property
 def Document(self):
  return self._document

 def __init__(self,url="",document=None,pyPreviousUrl="",name="",parent=None,screen=None,navigator=None,resolver=None,pageQ=None,fragment=0):
  classes="Attr CDATASection CharacterData Comment CSSStyleDeclaration DocumentFragment DocumentType DOMException DOMImplementation DOMStorage Element Entity EntityReference Event HTMLAppletElement HTMLBaseElement HTMLBodyElement HTMLButtonElement HTMLCollection HTMLElement HTMLFormElement HTMLFormElementWrapper HTMLFrameElement HTMLFrameSetElement HTMLHeadElement HTMLHtmlElement HTMLIFrameElement HTMLImageElement HTMLInputElement HTMLIsIndexElement HTMLLinkElement HTMLMetaElement HTMLOptGroupElement HTMLOptionElement HTMLScriptElement HTMLSelectElement HTMLStyleElement HTMLTableElement HTMLTableRowElement HTMLTableSectionElement HTMLTextAreaElement HTMLTitleElement NamedNodeMap Node NodeList Notation ProcessingInstruction Text TextRectangle"
  ds=set(dir(self))
  classes=[i for i in classes.split(" ") if i not in ds]
  for c in classes:
#   log("set "+str(c))
   setattr(self,c,globals()[c])
  self.events={}
  self.events_added=zeroDict()
  self._location=Location(url,self)
  self._document=document
  self.name=name
  self.navigator=navigator if navigator!=None else Navigator()
  self.screen=screen if screen!=None else Screen()
  self.console=Console()
  self.frames={}
#get frames from document upon instantiation (self.framesself.document.pyGetAllFrames())
  self.closed=0
  self._history=History(url=self.URL,previous="")
  self.locationbar=visibleConstruct()
  self.menubar=visibleConstruct()
  self.name=name
  self.opener=self 
  self.outterwidth,self.outterheight,self.pageXOffset,self.pageYOffset=1024,768,0,0
  self.parent=parent if parent!=None else self 
#defaultStatus, status
  self.personalbar=visibleConstruct()
#  self.prototype=box()
  self.scrollbars=visibleConstruct()
  self.self=self 
  self.statusbar=visibleConstruct()
  self.toolbar=visibleConstruct()
  self.window=self
  BaseClass.pageQ=pageQ
  ds=dir(self)
  self.resolver=resolver
  self.pageQ=pageQ
  self.fragment=fragment
  self.localStorage=DOMStorage(persistent=1)
  self.sessionStorage=DOMStorage(persistent=0)

 @property
 def history(self):
  return self._history

 def dump(self,*a):
  log("dump::",str(a))

 @property
 def length(self):
  return len(self.frames)

 @property
 def top(self):
  if self.parent==None or self.parent==self:
   return self 
  p=self.parent
  while p.parent!=None and p.parent!=p:
   p=p.parent
  return p

 def XMLHttpRequest(self):
  return XMLHttpRequest(window=self)

 def setDocument(self,document):
  self._document=document
  self.document.window=self

 def alert(self,msg):
  self.backend("js.alert",msg)

 def back(self):
  self.backend("js.navigateBack",self.pyPreviousUrl)

 def blur(self,win):
  self.backend("js.blur",self)

 def captureEvents(self,events):
  pass

 def clearInterval(self,intervalId):
  self.backend("js.clearInterval",id=intervalId)

 def clearTimeout(self,timeoutId):
  self.backend("js.clearTimeout",id=timeoutId)

 def close(self):
  self.backend("js.close")

 def confirm(self,msg):
  self.backendNoAsync("js.confirm",msg)

 def disableExternalCapture(self):
  pass

 def enableExternalCapture(self):
  pass

#fix for case
 def find(self,s="",casesensative=0,backward=0):
  return s in str(self.document)

 def focus(self):
  self.backend("js.focus",self)

 def forward(self):
  self.backend("js.navigateForward")

 def handleEvent(self,eventId):
  pass

 def home(self):
  self.backend("js.navigateHome")

 def image(self,width=0,hight=0):
  x=self.document.createElement("img")
  x.parentNode=self
  x.ownerDocument=self
  x._flags.hidden=1
  return x
 Image=image

 def moveBy(self,x,y):
  pass

 def moveTo(self,x,y):
  pass

#fix; parse options like (icecream=1 icecream=yes) to be equal as (icecream=1)
 def open(self,url,name="",features=""):
  self.backend("js.open",url,features=features)

 def _print(self):
  pass

 def prompt(self,msg,defaultText):
  self.backendNoAsync("js.prompt",msg,defaultText=defaultText)

 def releaseEvents(self):
  pass

 def resizeBy(self,x,y):
  pass

 def resizeTo(self,x,y):
  pass

 def routeEvent(self,eventType):
  pass

 def scroll(self,x):
  pass

 def scrollTo(self,x,y):
  pass

 def scrollBy(self,x,y):
  pass

 def setInterval(self,code,when=1000):
  if not when: when=1000
  rc=self.generateRandomCode()
  self.backend("js.setInterval",when=when,code=code,id=rc,dom=self)
  return rc

 def setTimeout(self,code,when=1000):
  if not when: when=1000
  rc=self.generateRandomCode()
  self.backend("js.setTimeout",when=when,code=code,id=rc,dom=self)
  return rc

 def stop(self):
  self.backend("js.stop")

 def toSource(self):
  pass

 def toString(self):
  return str(self)

 def unWatch(self,property):
  pass

 def watch(self,property):
  pass

 @property
 def URL(self):
  return str(self.location.href)

 @property
 def location(self):
  return self._location
 @location.setter
 def location(self,x):
  self.location.href=x

class Node(eventableElements,BaseClass):
 _type=None
 _display=[]
 _isNode=1
 _isElement=0
 _flags=utils.zeroDict()
# children=NodeList()
 ELEMENT_NODE = 1
 ATTRIBUTE_NODE = 2
 TEXT_NODE = 3
 CDATA_SECTION_NODE = 4
 ENTITY_REFERENCE_NODE = 5
 ENTITY_NODE = 6
 PROCESSING_INSTRUCTION_NODE = 7
 COMMENT_NODE = 8
 DOCUMENT_NODE = 9
 DOCUMENT_TYPE_NODE = 10
 DOCUMENT_FRAGMENT_NODE = 11
 NOTATION_NODE = 12

#newstr
 def __str__(self):
#  if self.ownerDocumentReal and self.ownerDocumentReal.all_nodes and self in self.ownerDocumentReal.all_nodes:
#   return self.__str_fast__()
  return self.__str_slow__()

 def __str_fast__(self):
  d=self.ownerDocumentReal.all_nodes
  start=d.index(self)
  end=self._nextSiblingAny(self)
  l=[]
  j=start-1
  while j+1<end:
   j+=1
   i=d[j]
   if type(i) in (str,unicode):
    l.append(i)
    continue
   if hasattr(i,"setTimeout"):
    continue
   it=i._typeI
   if it in (Node.DOCUMENT_NODE,Node.DOCUMENT_FRAGMENT_NODE):
    continue
   if it==Node.ELEMENT_NODE:
    try:
     i.tagName
    except:
     l.append(str(repr(i))+","+str(i)+","+str(type(i)))
     continue
    attrs=" ".join(['%s="%s"' % (str(k),str(v),) for k,v in i.attrs.items() if k not in i.event_names])
#    for name in i.events:
#     attrs+=" "+" ".join(['%s="%s"' % (str(name),str(j.original),) for j in i.events[name] if j.fromAttribute])
    attrs="" if attrs.strip()=="" else " "+attrs.strip()
    l.append("<%s%s>" % (i.tagName.lower(),attrs,))
    continue
   elif it==Node.TEXT_NODE:
    l.append(i.nodeValue)
    continue
   elif it==Node.CDATA_SECTION_NODE:
    l.append("<?--[[%s]]>" % (i.nodeValue,))
    continue
   elif it==Node.DOCUMENT_TYPE_NODE:
    l.append("<?doctype %s %s %s?>" % (i.publicId,i.systemId,i.name,))
    continue
   elif it==Node.ATTRIBUTE_NODE:
    l.append("%s=%s" % (str(i.nodeName.lower()),str(i.nodeValue),))
    continue
   elif it==Node.COMMENT_NODE:
    l.append("<!--%s-->" % (str(i.nodeValue),))
    continue
   elif it==Node.PROCESSING_INSTRUCTION_NODE:
    l.append("<?%s=%s?>" % (i.data),str(i.target,))
    continue
   else:
    l.append("<unknown>%s</unknown>" % (repr(i),))
  return u"".join([i.decode('utf-8',"ignore") for i in l])

 def __str_slow__(self):
  l=[]
  t=[self]
  while t:
   i=t.pop(0)
   if isinstance(i,basestring):
    l.append(str(i))
    continue
   if isinstance(i,window):
    continue
   if i._typeI==Node.DOCUMENT_NODE:
    [t.insert(0,i) for i in i.childNodes[::-1]]
   elif i._typeI==Node.DOCUMENT_FRAGMENT_NODE or i._typeI==Node.ELEMENT_NODE:
    try:
     i.tagName
    except:
     t.insert(0,str(repr(i))+","+str(i)+","+str(type(i)))
     continue
    attrs=" ".join(['%s="%s"' % (str(k),str(v),) for k,v in i.attrs.items() if k not in i.event_names])
#    for name in i.events:
#     attrs+=" "+" ".join(['%s="%s"' % (str(name),str(j.original),) for j in i.events[name] if j.fromAttribute])
    attrs="" if attrs.strip()=="" else " "+attrs.strip()
    t.insert(0,"</%s>" % (i.tagName.lower(),))
    if i.hasChildNodes():
     [t.insert(0,j) for j in i.childNodes[::-1]]
    t.insert(0,"<%s%s>" % (i.tagName.lower(),attrs,))
   elif i._typeI==Node.TEXT_NODE:
    t.insert(0,str(i.nodeValue))
   elif i._typeI==Node.CDATA_SECTION_NODE:
    t.insert(0,"<?--[[%s]]>" % (str(i.nodeValue),))
   elif i._typeI==Node.DOCUMENT_TYPE_NODE:
    t.insert(0,"<?doctype %s %s %s?>" % (i.publicId,i.systemId,i.name,))
   elif i._typeI==Node.ATTRIBUTE_NODE:
    t.insert(0,"%s=%s" % (str(i.nodeName.lower()),str(i.nodeValue),))
   elif i._typeI==Node.COMMENT_NODE:
    t.insert(0,"<!--%s-->" % (str(i.nodeValue),))
   elif i._typeI==Node.PROCESSING_INSTRUCTION_NODE:
    t.insert(0,"<?%s=%s?>" % (i.data),str(i.target,))
   else:
    t.insert(0,"<unknown>%s</unknown>" % (repr(i),))
  return u"".join([i.decode('utf-8',"ignore") for i in l])
#[i.encode('utf-8') for i in l])

 def x__str__(self):
  return str(self.nodeValue if self.nodeValue!=None else '')

 def iterNodes(self,includeEndings=0):
  t=collections.deque([self])
  if includeEndings==1:
   while len(t)>0:
    c=t.popleft()
    if c==None:
     continue
    if type(c)==window: #isinstance(c,window):
     c=c.document
    if type(c)==tuple: #isinstance(c,tuple):
     yield c
    else:
     if c._typeI!=3: t.appendleft(("end",c))
     [t.appendleft(i) for i in c.childNodes[::-1]]
     yield c
  else:
   while len(t)>0:
    c=t.popleft()
    if c==None:
     continue
    if type(c)==window: #isinstance(c,window):
     c=c.document
    [t.appendleft(i) for i in c.childNodes[::-1]]
    yield c

 def __repr__(self):
  return "%s <0x%08x>" % (self.nodeName if hasattr(self,"nodeName") else ''.lower(),id(self),)

 def compareDocumentPosition(self,other):
  if self.ownerWindow.document!=other.ownerWindow.document:
   return 1
  if self in other.parentNodesL:
   return 8
  if other in self.parentNodesL:
   return 10
  allNodes=self.ownerWindow.document._all
  this,that=allNodes.index(self),allNodes.index(other)
  if that>this:
   return 4
  if this>that:
   return 2
  return 20

 def contains(self,other):
  if other==self or self in other.parentNodesL:
   return 1
  return 0

 def hasAttribute(self,x):
  return x in self.attrs

 def getElementsByClassName(self,className="",level=0):
  l=HTMLCollection([])
  if not className: return l
  names=[i for i in className.split(" ") if i.strip()!=""] if isinstance(className,basestring) else className
  names=set(names)
#  log("getElementByClassName:",className)
  ls=self.iterNodes()
  for i in ls:
   if i._isElement and i.hasAttribute("class") and len([j for j in i.getAttribute("class").split(" ") if j in names]):
    l.append(i)
  return HTMLCollection(l)

 def getElementsByName(self,name):
#  log("element.gebn",name)
  return HTMLCollection([i for i in self._all if i._isElement and i.hasAttribute("name") and i.getAttribute("name")==name])

 def getElementsByTagName(self,tag,_return1=0):
#  log("gebtn",tag)
  if tag=="*":
   return HTMLCollection([i for i in self._all if i._isElement])
  if isinstance(tag,basestring): tag=[tag]
  tag=set([i.upper() for i in tag])
  ls=self.iterNodes()
  if _return1:
   for i in ls:
    if i._isElement and i.tagName in tag:
     del ls
     return i
  ret=HTMLCollection()
  for i in ls:
   if i._isElement and i.tagName in tag:
    ret.append(i)
#  if "script" in tag:
#   log("scripts with src:",str([i.src for i in ret if i.hasAttribute("src")]))
#  log("getelementsbytagname:::",str(self),"::",tag,"::",ret,"::",len(ret),"::",ret[0],"::end")
  return ret

 @property
 def tagName(self):
  return self.nodeName

# @property
# def all(self):
#  return self._all

 @property
 def _all(self):
  return HTMLCollection(self.iterNodes())

 @property
 def parentNodesL(self):
  l=[self]
#  log("pLog:",p,"hasattr:",hasattr(p,"parentNode"))
#  log("p:",p,"parentNode:",p.parentNode,"hasAttr:",hasattr(p,"parentNode"),"!=:",p!=p.parentNode,"!=None:",p.parentNode!=p)
  while l:
   p=l[-1]
   if hasattr(p,"parentNode") and p.parentNode not in (p,None):
    l.append(p.parentNode)
   else:
    break
  return l

 def parentNodes(self,count=0,attr=None,name=None,includeSelf=0):
  if name!=None and type(name) not in (set,list,tuple): name=[name]
  name=[i.upper() for i in name] if name else None
  l=[]
  if includeSelf==1:
   p=self
  else:
   p=self.parentNode
  while p!=None:
#   log("parent:",repr(p))
   if (attr==None or p.hasAttribute(attr)) and (name==None or p.nodeName in name):
    if count==1:
     return p
    else:
     l.append(p)
   p=p.parentNode
  if count!=1:
   return l
  else:
   return None

 @property
 def parentNodeNames(self):
  n=set()
  p=self
  while p!=None:
   n.add(p.nodeName)
   p=p.parentNode
  return n

 @property
 def attributes(self):
  return NamedNodeMap([self.getAttributeNode(k) for k in self.attrs])

 @property
 def baseName(self):
  return self.name

 @property
 def childNodes(self):
  return self.children #NodeList([i for i in self.children])

 @property
 def dataType(self):
  return self._typeI

 @property
 def definition(self):
  return None

 @property
 def firstChild(self):
  return self.children[0] if len(self.children)>0 else None

 @property
 def lastChild(self):
  return self.children[-1] if len(self.children)>0 else None

 def cloneNode(self,deep=0):
  nt=self.nodeType
  od=self.ownerWindow.document
  if nt==od.ELEMENT_NODE:
   n=od.createElement(self.tagName)
  elif nt==od.TEXT_NODE:
   n=od.createTextNode(self.nodeValue)
  elif nt==od.COMMENT_NODE:
   n=od.createComment(self.nodeValue)
  elif nt==od.DOCUMENT_FRAGMENT_NODE:
   n=od.createDocumentFragment()
  elif nt==od.ATTRIBUTE_NODE:
   n=od.createAttribute(self.nodeName,self.nodeValue)
  elif nt==od.CDATA_SECTION_NODE:
   n=od.createCDATASection(self.nodeValue)
  elif nt==od.DOCUMENT_NODE:
   n=od.createDocument()
  else:
   n=od.createElement(self.tagName)
#  n=self.ownerWindow.document.createElement(self.tagName)
  if hasattr(self,"setAttribute"):
   for k,v in self.attrs.items():
    n.setAttribute(k,v)
  top=n
  if deep==True:
   for c in self.childNodes:
    n.appendChild(c.cloneNode(deep=1))
  return n

 def hasChildNodes(self):
  return self.childNodes.length

 def hasAttributes(self):
  return len(self.attributes)

 def namespaceURI(self):
  return ""
#self.doc.url

 @property
 def nextSibling(self):
  try:
   s=self.parentNode.childNodes.index(self)
   l=self.parentNode.childNodes.length
   if s+1>=l:
    return None
  except:
   return None
  return self.parentNode.childNodes[s+1]

 @property
 def previousSibling(self):
  try:
   s=self.parentNode.childNodes.index(self)
   if s<=0: return None
  except:
   return None
  try:
   return self.parentNode.childNodes[s-1]
  except:
   return None

 @property
 def nodeName(self):
  if self._type=="element":
   return self._name.upper()
  elif self._type=="attr":
   return self._name
  elif self._type=="comment":
   return "#comment"
  elif self._type=="text":
   return "#text"
  elif self._type=="document":
   return "#document"
  elif self._type=="fragment":
   return "#document-fragment"
  return None
 @nodeName.setter
 def nodeName(self,x):
  if self._type=="element":
   self._name=x
  elif self._type=="attr":
   self._name=x
  else:
   raise Exception
  return self._name

 @property
 def _nodeText(self):
  v=self.nodeValue
  if v==None: return ''
  return v if not v.isspace() else ''

 @property
 def nodeValue(self):
  if self._type in ("text","comment","attr"):
   return self._value
  else:
   return None
 @nodeValue.setter
 def nodeValue(self,x):
  self._value=x

 @property
 def nodeType(self):
  return self._typeI

 @property
 def ownerDocument(self):
  return self._ownerDocument
 @ownerDocument.setter
 def ownerDocument(self,x):
  self._ownerDocument=x
  self._ownerWindow=getattr(x,"window",None)
 @property
 def ownerWindow(self):
  return self._ownerWindow
 @property
 def ownerDocumentReal(self):
  return self._ownerDocument
 @property
 def parentNode(self):
  return self._parent
 @parentNode.setter
 def parentNode(self,x):
  self._parent=x
  return x
 @property
 def parentElement(self):
  return self.parentNode if self.parentNode._type=="element" else None
 @property
 def prefix(self):
  return ""

 @property
 def textContent(self):
  l=[]
  for i in self._all:
   if i._type=="text":
    l.append(i.nodeValue)
   else:
    continue
  return ''.join(l)
 @textContent.setter
 def textContent(self,s):
  while self.childNodes.length:
   self.removeChild(self.childNodes[0])
  self.appendChild(self.ownerWindow.document.createTextNode(s))
 def __init__(self,name="",_ownerDocument=None):
#~~
#  self.wbDirSelf=set(dir(self))
#  log("node.__init__",str(name),str(_attrs),repr(self))
  self.events={}
  self.events_added=zeroDict()
  self.children=NodeList()
  self._flags=utils.zeroDict()
  self.attrs={}
  self._parent=None
  self._ownerDocument=_ownerDocument
  name='' if not name else name
  if self._type in ("text","comment","cdata"):
   self.nodeValue=name
  else:
   self._name=name.lower()

 def _adoptNode(self,o):
  o.parentNode=self
  return o

 def _takeOwnership(self,o,l=0,recursive=1,onlySubelementsGetParent=0):
#  log("takeOwnership:",repr(o))
#  log("_takeOwnership",repr(o),type(o),o._flags,repr(getattr(self.ownerDocument,"window",None)))
  try:
   o.ownerDocument=self.ownerDocumentReal
   od=o.ownerDocumentReal
  except:
   od=None
   pass
  z='''
  od=None
  try:
   ow=self.ownerWindow
   od=ow.document
   o.ownerDocument=od
  except:
   pass
'''
  try:
   o.ownerDocumentReal._current=o
  except:
   pass
  if isinstance(o,window):
   return self._takeOwnership(o.document)
  if o._type=="fragment":
   [self.appendChild(i) for i in o.childNodes]
   self.removeChild(o)
   return o.childNodes[0]
  o.parentNode=self
  if 1==0 and (o.tagName=="FRAME" or o.tagName=="IFRAME") and not o._flags.hasParsed and self.ownerWindow.document!=None:
   src=o.getAttribute("src")
   src=makeFullLink(src,self.ownerWindow.document._base)
   self.insertBefore(parseString(self.ownerWindow.resolver(src,dataOnly=1)),o)
   self.removeChild(o)
   o._flags.hasParsed=1
  if self.ownerDocumentReal and self.ownerDocumentReal.window:
   if o.tagName=="SCRIPT" and o.getAttribute("type") in ("text/javascript",None) and not o._flags.hasRun and od!=None and (o.textContent.strip()!="" or o.hasAttribute("src")):
 #o._flags.is_parser_inserted and not o._flags.hasRun and not o._flags.is_fragment:
    if o.hasAttribute("src") and not o._flags.nofetch and o.getAttribute("src")!="":
     url=makeFullLink(o.getAttribute("src"),self.ownerDocumentReal._base)
 #    log("scriptWinObject:",repr(self.ownerWindow.document.window),"scriptResolverObject",repr(self.ownerWindow.resolver),"winDir",dir(self.ownerWindow.document.window))
 #    if self.ownerDocument.readyState=="loading" and not self._flags.gotsource:
 #     self.ownerDocument._parser.addUrlToDom(url,self)
 #     return
     try:
      data=self.ownerWindow.resolver(url)
      txt='' if data.status_code>=300 else data.content
      if data.status_code>=300 or data.status_code<200: log("err:takeOwnership:",data.status_code,str(data))
      o.appendChild(self.ownerDocumentReal.createTextNode(txt))
     except Exception,e:
      o.error()
      log("ErrWithDynamicUrl:",url,"exception:",str(e)); txt=""
    try:
 #    log("script running o",str(o))
     o.eval()
     if "load" in o.events: o.evt_load()
    except Exception,e:
     log("ScriptExecuteErrorFromNodeTakeOwnership",e); generate_error_report(e)
    o._flags.hasRun=1
  o.updateActiveAttrs()
  if recursive==1:
   for i in o.childNodes:
    o._takeOwnership(i,recursive=1,l=l+1)
#  log("_takeOwnershipDone",str(o),type(o))
  return o

 def appendChild(self,o,recursive=0):
#  if not isinstance(o,window) and set(o.parentNodes()).intersection(set(self.parentNodes())):
#   raise DOMException(DOMException.HIERARCHY_REQUEST_ERR)
  if o in self.parentNodesL:
   raise
  if o.ownerDocument==self.ownerDocument and o.parentNode and o in o.parentNode.childNodes:
   pass #log("Attempt to move an element:",repr(o),str(o),"o.ownerDocument",repr(o.ownerDocument),"self",repr(self),str(self),"o.parentNode",repr(o.parentNode),str(o.parentNode))
   o=o.parentNode.removeChild(o)
#  log("appending",repr(o),"to node",repr(self),"self.ownerDocReal",repr(self.ownerDocumentReal))
  self.children.append(o)
  o=self.children[len(self.children)-1]
#  log("taking ownership")
  if self.ownerDocumentReal in self.parentNodesL:
   self._appendChild(o)
  x=self._takeOwnership(o,recursive=recursive)
#  log("took ownership")
  return x

 def _nextSiblingAny(self,o):
  index=-2
  p=o
  while p.nextSibling==None:
   p=p.parentNode
   if p==None:
    index=-1
    break
  if index!=-1 and p.nextSibling in self.ownerDocumentReal.all_nodes:
   index=self.ownerDocumentReal.all_nodes.index(p.nextSibling)
  else:
   index=len(self.ownerDocumentReal.all_nodes)
  return index

 def _appendChild(self,o):
  index=self._nextSiblingAny(self)
  self.ownerDocumentReal.all_nodes.insert(index,o)
  self._addAllChildren(o,index+1)

 def _addAllChildren(self,o,index):
  if o.childNodes:
   l=list(o.iterNodes())[1:]
   for i in l:
    self.ownerDocumentReal.all_nodes.insert(index,i)
    index+=1

 def removeChild(self,old):
  if old==self or self.childNodes.length==0:
   raise Exception
  if self.ownerDocumentReal in self.parentNodesL:
   self._removeChild(old)
  self.children.remove(old)
  old.parentNode=None
  return old

 def _removeChild(self,o):
  indexOld=self.ownerDocumentReal.all_nodes.index(o)
  index=self._nextSiblingAny(o)
  d=self.ownerDocumentReal.all_nodes
  self.ownerDocumentReal.all_nodes=d[:indexOld]+d[index:]

 def insertBefore(self,new,current,recursive=0):
  log("insertBefore")
#  raise DOMException(DOMException.HIERARCHY_REQUEST_ERR)
  if current==None:
   return self.appendChild(new,recursive=recursive)
  ci=self.children.index(current)
  try:
   index=self.ownerDocumentReal.all_nodes.index(current)
  except:
   index=len(self.ownerDocumentReal.all_nodes)
  self.children.insert(ci,new)
  if self.ownerDocumentReal in self.parentNodesL:
   self._insertBefore(index,new)
  new=self._takeOwnership(self.children[ci],recursive=recursive)
  return new

 def _insertBefore(self,index,new):
  log("_insertBefore",repr(new))
  self.ownerDocumentReal.all_nodes.insert(index,new)
  self._addAllChildren(new,index+1)

 def replaceChild(self,new,old):
  log("replaceChild")
  idx=self.childNodes.index(old)
  self.removeChild(old)
  if self in new.parentNodesL:
   new.parentNode=self.parentNode
  if self.childNodes.length<=idx:
   self.appendChild(new)
  else:
   self.insertBefore(new,self.childNodes[idx])
#  log("replaceChild done")
  return old

 def normalize(self):
  p=self.firstChild
  if p!=None:
   p.normalize()
   n=p.nextSibling
   while n!=None:
    if n._type=="text" and p._type=="text":
     s=p.nodeValue+n.nodeValue
     t=Text(s)
#check
     p.parentNode.insertBefore(t,p)
     self.removeChild(n)
     n=p.nextSibling
    else:
     n.normalize()
     p=n
     n=n.nextSibling

 def isEqualNode(self,n2):
  n1=self 
  if n1==n2: return True
  if n1==None or n2==None: return False
  if n1.nodeType!=n2.nodeType: return False
  if n1.nodeName!=n2.nodeName: return False
  if n1.childNodes.length!=n2.childNodes.length: return False
  if n1.attributes.length!=n2.attributes.length: return False
  n1a=n1.attributes
  n2a=n2.attributes
  if len([i for i in n1a if n2a.get(i,None)==n1a[i]])!=n1a.length: return False
  n1c=n1.childNodes
  n2c=n2.childNodes
  for i in xrange(n1c.length):
   if not n1c[i].isEqualNode(n2c[i]): return False

 def isSameNode(self,o):
  if self==o: return True
  if self==None or o==None: return False

 def isSupported(self,feature,version):
  return False

class ProcessingInstruction(Node):
 def x__str__(self):
  return "<?"+str(self.data)+"="+str(self.target)+"?>"

 _type="processingInstruction"
 _typeI=Node.PROCESSING_INSTRUCTION_NODE
 @property
 def data(self):
  return self._data
 @data.setter
 def data(self,x):
  self._data=x
 @property
 def target(self):
  return self._target
 @target.setter
 def target(self,x):
  self._target=x

class DocumentType(Node):
 _type="documentType"
 _typeI=Node.DOCUMENT_TYPE_NODE
 def x__str__(self):
  return "<?doctype %s %s %s?>" % (self.publicId,self.systemId,self.name,)

 def __init__(self,publicId="",systemId="",qualifiedName=""):
  super(DocumentType,self).__init__()
  self._publicId=publicId
  self._systemId=systemId
  self._qualifiedName=qualifiedName
 @property
 def systemId(self):
  return self._systemId
 @property
 def publicId(self):
  return self._publicId
 @property
 def name(self):
  return self._qualifiedName
 @property
 def internalSubset(self):
  return None

class DOMImplementation(BaseClass):
 def __str__(self):
  return "<DOMImplementation %08x" % (id(self),)

 def __init__(self,_ownerDocument=None):
  self.ownerDocument=_ownerDocument

 def createDocument(self,namespaceURI="",qualifiedNameStr="",docType=""):
  d=document()
  d.attrs["namespaceURI"]=namespaceURI
  d.attrs["qualifiedNameStr"]=qualifiedNameStr
  d.attrs["docType"]=docType
  return d

 def createDocumentType(self,qualifiedNameStr='',publicId='',systemId=''):
  return DocumentType(qualifiedName=qualifiedNameStr,publicId=publicId,systemId=systemId) 

 def createHTMLDocument(self,title):
  d=self.domimpl.createDocument("http://www.w3.org/1999/xhtml","html",self.ownerWindow.document.documentElement.docType)
  [d.appendChild(i) for i in self._parseToNodes("<html><head><title>%s</title></head><body></body></html>" % (title,)).childNodes]
  return d

 def getFeature(self,feature,version):
  return None

 def hasFeature(self,feature,version):
  return 0

class CharacterData(Node):
 def __getitem__(self,x):
  return self.nodeValue.__getitem__(x)
 def __setitem__(self,x,y):
#  log("characterData:setitem:",repr(x),repr(y))
  return self.nodeValue.__setitem__(x,y)
 def __getslice__(self,x,y,z):
  return self.nodeValue.__getslice__(x,y,z)
 def x__str__(self):
  return str(self.data)
 @property
 def nodeValue(self):
  return self._value
 @nodeValue.setter
 def nodeValue(self,x):
  self._value=str(x)
 @property
 def data(self):
  return self.nodeValue
 @data.setter
 def data(self,x):
  self.nodeValue=x
 @property
 def __len__(self):
  return self.length
 def length(self):
  return len(self.nodeValue)
 def appendData(self,x):
  self.nodeValue+=x
 def deleteData(self,offset,count):
  s=self.nodeValue
  self.nodeValue=s[:offset]+s[offset+count:]
 def insertData(self,offset,s):
  self.data=self.data[:offset]+s+self.data[offset:]
 def replaceData(self,offset,count,s):
  self.deleteData(offset,count)
  self.insertData(offset,s)
 def substringData(self,offset,count):
  return self._value[offset:offset+count]

class Attr(Node):
 def x__str__(self):
  return self.nodeName+"="+str(self.nodeValue)

 _type="attr"
 _typeI=Node.ATTRIBUTE_NODE
 @property
 def attributes(self):
  return None
 @property
 def firstChild(self):
  return None
 @property
 def lastChild(self):
  return None
 @property
 def nextSibling(self):
  return None
 @property
 def previousSibling(self):
  return None
 @property
 def parentNode(self):
  return None
 @property
 def name(self):
  return self.nodeName
 @property
 def ownerElement(self):
  return self._parent if hasattr(self,"_parent") else None
 @property
 def specified(self):
  return 1
 @property
 def value(self):
  return self.nodeValue
 @value.setter
 def value(self,x):
  self._value=x
  return self._value
 @property
 def isId(self):
  return 1 if self.nodeName=="ID" else 0

class Comment(CharacterData):
 def x__str__(self):
  return "<!--"+str(self.nodeValue)+"-->"
 _type="comment"
 _typeI=Node.COMMENT_NODE

class CDATASection(CharacterData):
 def x__str__(self):
  return "<!--[[%s]]>" % (str(self.nodeValue),)
 _type="cdata"
 _typeI=Node.CDATA_SECTION_NODE

class Text(CharacterData):
 def x__str__(self):
  return str(self.nodeValue)
 _type="text"
 _typeI=Node.TEXT_NODE
 @property
 def wholeText(self):
  return self.parentNode.textContent
 @property
 def isElementContentWhitespace(self):
  return len((i for i in self.nodeValue if i not in string.whitespace))==0
 def splitText(self,offset):
  self.nodeValue,new=self.nodeValue[0:offset],self.nodeValue[offset:]
  t=Text(new)
#self.document.createElement(_type="text",_parent=self.parent,_value=new)
  self.parentNode.insertBefore(t,self)
  return t

class DocumentFragment(Node):
 _type="fragment"
 _typeI=Node.DOCUMENT_FRAGMENT_NODE

class Notation(Node):
 _type="notation"
 _typeI=Node.NOTATION_NODE
 @property
 def publicId(self):
  pass
 @property
 def systemId(self):
  pass

class Entity(Node):
 _type="entity"
 _typeI=Node.ENTITY_NODE
 @property
 def publicId(self):
  pass
 @property
 def systemId(self):
  pass
 @property
 def notationName(self):
  pass
 
class EntityReference(Node):
 _type="entityReference"
 _typeI=Node.ENTITY_REFERENCE_NODE

class NodeList(list,BaseClass):
 def __init__(self,*elements):
  list.__init__(self,*elements)
 @property
 def length(self):
  return len(self)
 def item(self,index):
  try:
   return super(NodeList,self).__getitem__(index)
  except Exception,e:
   log("errList::",str(index),str(e))
   return None

class Sheet(object):
 rules=[]
 def insertRule(self,rule,id):
  self.rules.append((id,rule))
 def deleteRule(self,id):
  for i in rules:
   if i[0]==id:
    self.rules.remove(i)
   return i

class HTMLCollection(NodeList):
 def __getitem__(self,x):
  if str(x).isalpha():
   return self.namedItem(x)
  else:
   return self.item(x)

 def namedItem(self,name):
  for i in self:
   if i.hasAttribute("id") and i.getAttribute("id")==name: return i
  for i in self:
   if i._type=="element" and i.getAttribute("name")==name: return i

class NamedNodeMap(list,BaseClass):
 @property
 def length(self):
  return len(self)
 def getNamedItem(self,x):
  ret=[i for i in self if i.nodeName==x]
  return ret[0] if ret else None
 def setNamedItem(self,x):
  ret=[i for i in self if i.nodeName==x.nodeName]
  if ret:
   self[self.index(ret)]=x
  else:
   self.append(x)
 def removeNamedItem(self,x):
  elem=[i for i in self if i.nodeName==x]
  if elem:
   return self.pop(self.index(elem))
  return None
 def item(self,x):
  return list.__getitem__(self,x)
 def __getattr__(self,x):
  return self.__getitem__(x)
 def __getitem__(self,x):
  return self.item(x) if str(x).isdigit() else self.getNamedItem(x)

class CSSStyleDeclaration(BaseClass):
 _quotes=['"',"'"]
 def update(self):
  if self.elem:
   self.elem.setAttribute("style",self.cssText)
 def __init__(self, style,elem):
  self.elem=elem
  parts=[i.split(":",1) for i in style.split(";") if ":" in i]
  parts=[(k.strip(),v[1:-1].strip() if v[0]==v[-1] and v[0] in self._quotes else v.strip()) for k,v in parts]
  self.attrs = dict(parts)
  self.update()
 def __len__(self):
  return self.length
 @property
 def length(self):
  return len(self.attrs)
 def removeAttribute(self,x):
  ret=''
  if x in self.attrs:
   ret=self.attrs.pop(x)
   self.update()
  return ret

 @property
 def cssText(self):
  return '; '.join(["%s: %s" % (k, v) for k, v in self.attrs.items()])+"; " if self.attrs else ''
 @cssText.setter
 def cssText(self,x):
  for k,v in [j.split(":",1) for j in x.split(";") if ":" in j]: self.attrs[k]=v
  self.update()
  return self.cssText
 def getPropertyValue(self, name):
  return self.attrs.get(name, '')
 def removeProperty(self, name):
  ret=''
  if name in self.attrs:
   ret=self.attrs.pop(name)
   self.update()
  return ret
 def item(self, index):
  if type(index) == str:
   return self.attrs.get(index, '')
  if index < 0 or index >= len(self.attrs):
   return ''
  return self.attrs[self.attrs.keys()[index]]
 def __setitem__(self,key="",val=""):
#  log("css:setitem:",repr(key),repr(val))
  self.attrs[key]=val
  self.update()
 def __getitem__(self,x=""):
  return self.attrs.get(x,None)

#class ElementCSSInlineStyle(object):
class Element(Node):
 _isElement=1
 _type="element"
 _typeI=Node.ELEMENT_NODE
 clientWidth=prop("clientWidth",attrtype=int,default=0)
 clientTop=prop("clientTop",attrtype=int,default=0)
 clientHeight=prop("clientHeight",attrtype=int,default=0)
 clientLeft=prop("clientLeft",attrtype=int,default=0)
 offsetWidth=prop("offsetWidth",attrtype=int,default=0)
 offsetTop=prop("offsetTop",attrtype=int,default=0)
 offsetHeight=prop("offsetHeight",attrtype=int,default=0)
 offsetLeft=prop("offsetLeft",attrtype=int,default=0)
 scrollWidth=prop("scrollWidth",attrtype=int,default=0)
 scrollTop=prop("scrollTop",attrtype=int,default=0)
 scrollHeight=prop("scrollHeight",attrtype=int,default=0)
 scrollLeft=prop("scrollLeft",attrtype=int,default=0)
 prefix=prop("prefix",default="")

 def focus(self):
  pass

 def blur(self):
  pass

#ElementCSSInlineStyle,Node):
 def __delitem__(self,x):
  if x in self.attrs:
   self.removeAttribute(x)

 @property
 def localName(self):
  return self.tagName.lower()
 @localName.setter
 def localName(self,x):
  self.nodeName=x
 @property
 def namespaceURI(self):
  return getattr(self,'_namespaceURI','http://www.w3.org/1999/xhtml')
 @namespaceURI.setter
 def namespaceURI(self,x):
  self._namespaceURI=x
 @property
 def baseURI(self,x):
  return str(self.ownerWindow.document._base) if self.ownerWindow.document else ''
 @property
 def offsetParent(self):
  return self.parentNode
 def getBoundingClientRect(self):
  return TextRectangle()
 @property
 def style(self):
  v=CSSStyleDeclaration(self.attrs.get("style",""),self)
#  self.setAttribute("_css",v)
  return v
 def getAttribute(self,x):
#  log("getAttribute",x)
  return self.attrs.get(x,None)
 def getAttributeNode(self,x):
  ret=None
  if x in self.attrs:
   ret=Attr(x)
   ret.nodeValue=self.attrs.get(x,None)
  return ret
 def hasAttribute(self,x):
#  log("hasAttribute:"+str(repr(self))+":"+str(x))
  return x in self.attrs
 def removeAttribute(self,x):
  if x in self.attrs: self.attrs.pop(x)
 def removeAttributeNode(self,node):
  if node.nodeName in self.attrs: self.attrs.pop(node.nodeName)
 def setAttribute(self,k,v):
#  log("setattr:",str(k),str(v),str(repr(self)))
  self.attrs[k]=v
 def setAttributeNode(self,n):
  self.attrs[n.nodeName]=n.nodeValue
 def setIdAttribute(self,id):
  self.setAttribute("id",id)
 def setIdAttributeNode(self,idNode):
  self.setAtribute("id",idNode.nodeValue)

 def querySelector(self,*x):
  log("querySelector:"+str(x))
  return NodeList()

 def querySelectorAll(self,*x):
  log("querySelectorAll:"+str(x))
  return NodeList()

class HTMLElement(Element): 
 id = prop("id",default='')
 title = prop("title")
 lang = prop("lang")
 dir = prop("dir")
 className = prop("class",default="")
 
 @property
 def innerHTML(self):
  if not self.hasChildNodes():
   return ""
  return "".join([str(i) for i in self.childNodes])

 @innerHTML.setter
 def innerHTML(self, html):
  res=self._parseToNodes(str(html) if html else '')
#  log("got parsed result")
  while self.childNodes.length:
   self.removeChild(self.childNodes[0])
#  log("setting html:",repr(res))
  [self.appendChild(i,recursive=1) for i in res]

 @property
 def outerHTML(self):
  x=str(self)
  log("outerHTML called",x)
  return x
#  if not self.hasChildNodes():
#   return ""
#  return "".join([str(i) for i in self.childNodes])

 @outerHTML.setter
 def outerHTML(self, html):
  html=str(html) if html else ''
  log("setting html:",len(html))
  index=self.nextSibling
  p=self.parentNode
  action=p.insertBefore if index==None else p.appendChild
  direction=1 if index==None else -1
  self.parentNode.removeChild(self)
#~~
#  [self.removeChild(i) for i in self.childNodes]
  [action(i,index,recursive=1) for i in p._parseToNodes(html)[::direction]]

class HTMLHtmlElement(HTMLElement):
 version = prop("version")

class HTMLHeadElement(HTMLElement):
 profile = prop("profile")
 
class HTMLLinkElement(urlDecomposition,HTMLElement):
 disabled = prop("disabled",default=0,attrtype=bool)
 charset = prop("charset")
 href = prop("href")
 hreflang = prop("hreflang")
 media = prop("media")
 rel = prop("rel")
 rev = prop("rev")
 target = prop("target")
 type = prop("type")
 def getUrl(self):
  return makeFullLink(self.href,self.ownerWindow.document._base)
 def setUrl(self,x):
  self.href=x

class HTMLTitleElement(HTMLElement):
 @property
 def title(self):
  return self.childNodes[0] if self.childNodes.length==1 else Text("")
 @title.setter
 def title(self,x):
  if self.childNodes.length==1:
   self.childNodes[0].nodeValue=x
  if self.childNodes.length==0:
   self.appendChild(Text(x))
  else:
   while self.childNodes.length:
    self.removeChild(self.childNodes[0])
   self.title=x

class HTMLMetaElement(HTMLElement):
 content = prop("content")
 httpEquiv = prop("http-equiv")
 name = prop("name")
 scheme = prop("scheme")

class HTMLBaseElement(HTMLElement):
 href = prop("href")
 target = prop("target")

class HTMLIsIndexElement(HTMLElement):
 form = None
 prompt = prop("prompt")
 
class HTMLStyleElement(HTMLElement):
 disabled = False
 media = prop("media")
 type = prop("type")
 @property
 def sheet(self):
  if not hasattr(self,"_sheet"):
   self._sheet=Sheet()
   self._sheet._parent=self
  return self._sheet
 @property
 def styleSheet(self):
  return self.style

class HTMLBodyElement(HTMLElement):
 def __len__(self):
  return len(self.attributes)
 def __getitem__(self,x):
  if x.isdigit():
   return self.attrs.keys()[x]
  else:
   return getattr(self,x)
 background = prop("background")
 bgColor = prop("bgcolor")
 link = prop("link")
 aLink = prop("alink")
 vLink = prop("vlink")
 text = prop("text")
 
class HTMLFormElement(HTMLElement):
 def __getitem__(self,x):
  if x in [i.name for i in self.elements]:
   return [i for i in self.elements if i.name==x][0]
  elif x in [i.id for i in self.elements]:
   return [i for i in self.elements if i.id==x][0]
  else:
   return None
  return None
#raise NameError(x)
#  raise NameError(x)

 @property
 def elements(self):
  return self.getElementsByTagName(formElements)

 @property
 def length(self):
  return self.elements.length

 name = prop("name",default='')
 acceptCharset = prop("accept-charset", default="UNKNOWN")
 action = prop("action")
 enctype = prop("enctype", default="application/x-www-form-urlencoded")
 method = prop("method", default="get")
 target = prop("target")

 def submit(self,buttonPressed=None):
  vars={}
  blacklist=[]
  btname=buttonPressed.name if buttonPressed else None
  for i in self.getElementsByTagName(formElements):
   if not i.hasAttribute("name"):
    continue
   if buttonPressed and i.type=="submit":
    if not (btname and i.name==btname):
     continue
   if buttonPressed and i.nodeName=="BUTTON":
    if i.name!=btname:
     continue
   t=i.nodeName
   if not i.hasAttribute("name") or i.name==None: continue
   if i.name in blacklist:
    continue
   if (t=="input" and (i.type=="text" or i.type=="hidden")) or t in ("textarea","submit","select"):
       vars[i.name]=i.value
   elif t=="INPUT" and i.type=="radio":
    if i.checked==1:
     vars[i.name]=i.value
     blacklist.append(i.name)
   elif t=='TEXTAREA':
    vars[i.name]=i.innerHtml
   else:
    vars[i.name]=i.value
  vars_items=vars.items()
  vars={}
  for k,v in vars_items:
   vars[k]=v.strip() if v!=None else ''
#   vars['__vars__']=str(vars)
  url=makeFullLink(self.action if self.hasAttribute("action") and self.action!="" else self.ownerWindow.document.location.href,self.ownerWindow.document._base)
  method=self.method.lower()
  log("vars::",str(vars))
  if method=="post":
   self.backend("go",url=url,method=method,data=vars)
  else:
   self.backend("go",url=url,method=method,params=vars)

 def reset(self):
  return None
 
class HTMLFormElementWrapper(HTMLElement):
 @property
 def form(self):
  f=self.parentNodes(name="form",count=1)
  if f!=None:
   return f
  return None
  
class HTMLSelectElement(HTMLFormElementWrapper):
 @property
 def type(self):
  return "select-one" #(or select-multiple)

 selectedIndex = 0
 value = prop("value",str)
 
 @property
 def length(self):
  return self.options.length
#raise NotImplementedError()
  
 @property
 def options(self):
  return self.getElementsByTagName("option")
#raise NotImplementedError()
  
 disabled = prop("disabled", bool)
 multiple = prop("multiple", bool) 
 name = prop("name",default='')
 size = prop("size", long)
 tabIndex = prop("tabindex", long)
 
 def add(self, element, current=None):
  if element.nodeName!="OPTION":
   return None
#raise NotImplementedError()
  if current==None: current=len(self.options)
  self.insertBefore(element,current)
  
 def remove(self, index):
  if len(self.options)<=index:
   return None
#raise NotImplementedError()
  self.options[index].parentNode.removeChild(self.options[index])
  
class HTMLOptGroupElement(HTMLFormElementWrapper):
 disabled = prop("disabled", bool) 
 label = prop("label")
 
class HTMLOptionElement(HTMLFormElementWrapper):
 defaultSelected = prop("selected", bool) 
 index = prop("index", long)
 disabled = prop("disabled", bool) 
 label = prop("label")
 selected = prop("selected",bool,0)
 value = prop("value")
 @property
 def text(self):
  return self.nodeValue

class HTMLInputElement(HTMLFormElementWrapper):
 defaultValue = prop("value")
 defaultChecked = prop("checked")
 accept = prop("accept")
 accessKey = prop("accesskey")
 align = prop("align")
 alt = prop("alt")
 checked = prop("checked", bool)
 disabled = prop("disabled", bool)
 maxLength = prop("maxlength", long, default=sys.maxint)
 name = prop("name",default='')
 readOnly = prop("readonly", bool)
 size = prop("size")
 tabIndex = prop("tabindex", long)
 type = prop("type", default="text")
 useMap = prop("usermap")
 value=prop("value",default="")

#abstractmethod for value goes here

class HTMLTextAreaElement(HTMLFormElementWrapper):
 defaultValue = None
 accessKey = prop("accesskey")
 cols = prop("cols", long)
 disabled = prop("disabled", bool)
 name = prop("name",default='')
 readOnly = prop("readonly", bool)
 rows = prop("rows", long)
 tabIndex = prop("tabindex", long)

 @property
 def value(self):
  if self.childNodes.length>0:
   return self.childNodes[0].nodeValue
 @value.setter
 def value(self,x):
  while self.childNodes.length:
   self.removeChild(self.childNodes[0])
  self.appendChild(self.ownerWindow.document.createTextNode(x))

 @property
 def type(self):
  return "textarea"
 
class HTMLButtonElement(HTMLFormElementWrapper):
 accessKey = prop("accesskey")
 disabled = prop("disabled", bool)
 name = prop("name",default='')
 tabIndex = prop("tabindex", long)
 type = prop("type")
 value = prop("value")
 
class HTMLAppletElement(HTMLElement):
 align = prop("align")
 alt = prop("alt")
 archive = prop("archive")
 code = prop("code")
 codeBase = prop("codebase")
 height = prop("height")
 hspace = prop("hspace")
 name = prop("name",default='')
 object = prop("object")
 vspace = prop("vspace")
 width = prop("width")
 
class HTMLImageElement(HTMLElement):
 align = prop("align")
 alt = prop("alt")
 border = prop("border")
 height = prop("height")
 hspace = prop("hspace")
 isMap = prop("ismap")
 longDesc = prop("longdesc")
 lowSrc = prop("lowsrc")
 name = prop("name")
 src = prop("src")
 useMap = prop("usemap")
 vspace = prop("vspace")
 width = prop("width")
 
class HTMLScriptElement(HTMLElement):
 @property
 def text(self):
  if self.childNodes.length==0:
   t=Text("")
   self.appendChild(t)
   return self.childNodes[0].nodeValue

 @text.setter
 def text(self,x):
  if self.childNodes.length==1:
   self.childNodes[0].nodeValue=x
  elif self.childNodes.length==0:
   self.appendChild(Text(x))
  else:
   while self.childNodes.length:
    self.removeChild(self.childNodes[0])
   self.text=x

 htmlFor = None
 event = None
 charset = prop("charset")
 defer = prop("defer", bool)
 src = prop("src")
 type = prop("type")
 
class HTMLFrameSetElement(HTMLElement):
 cols = prop("cols")
 rows = prop("rows")

class HTMLFrameElement(HTMLElement):
 frameBorder = prop("frameborder")
 longDesc = prop("longdesc")
 marginHeight = prop("marginheight")
 marginWidth = prop("marginwidth")
 name = prop("name")
 noResize = prop("noresize", bool)
 scrolling = prop("scrolling")
 src = prop("src")
 
class HTMLTableElement(HTMLElement):
 @property
 def tBodies(self):
  return HTMLCollection([i for i in self.childNodes if i.tagName=="TBODY"])
 @property
 def rows(self):
  return self.getElementsByTagName("tr")
 @property
 def caption(self):
  try:
   return [i for i in self.childNodes if i.tagName=="CAPTION"][0]
  except:
   return None
 @property
 def tHead(self):
  try:
   return [i for i in self if i.nodeName=="THEAD"][0]
  except:
   return None
 @property
 def tFoot(self):
  try:
   return [i for i in self if i.nodeName=="TFOOT"][0]
  except:
   return None
 def createCaption(self):
  self.childNodes.insertBefore(self.ownerWindow.document.createElement("caption"),self.childNodes[0])
 def createTHead(self):
  self.childNodes.insertBefore(self.ownerWindow.document.createElement("thead"),self.childNodes[0])

 def insertRow(self,index=-1):
  if index==-1:
   return self.appendChild("tr")
  else:
   return self.insertBefore("tr",self.childNodes[index])

class HTMLTableSectionElement(HTMLElement):
 @property
 def rows(self):
  return HTMLCollection([i for i in self.childNodes if i.tagName=="TR"])

class HTMLTableRowElement(HTMLElement):
 @property
 def cells(self):
  return HTMLCollection([i for i in self.childNodes if i.tagName=="TD"])
 @property
 def rowIndex(self):
  return self.parentNode.parentNode.getElementsByTagName("tr").index(self)
 @property
 def sectionRowIndex(self):
  return self.parentNode.childNodes.index(self)

class HTMLIFrameElement(HTMLElement):
 align = prop("align")
 frameBorder = prop("frameborder")
 height = prop("height")
 longDesc = prop("longdesc")
 marginHeight = prop("marginheight")
 marginWidth = prop("marginwidth")
 name = prop("name") 
 scrolling = prop("scrolling")
 src = prop("src")
 width = prop("width")

def xpath_property(xpath, readonly=False):
 import re
 RE_INDEXED = re.compile("(\w+)\[([^\]]+)\]")
 parts = xpath.split('/')

 def getChildren(parent, parts, recursive=False):
  if len(parts) == 0:
   return [parent]
  part = parts[0]
  if part == '':
   return getChildren(parent, parts[1:], True)
  if part == 'text()':
   return str(parent)
  m = RE_INDEXED.match(part)
  if m:
   name = m.group(1)
   idx = m.group(2)
  else:
   name = part
   idx = None
  children = []
  tags = parent.getElementsByTagName(name, recursive=recursive)
  if idx:
   if idx[0] == '@':
    tags = [tag for tag in tags if tag.hasAttribute(idx[1:])]
   else:
    idx=int(idx)
    if len(tags)<=idx:
     return NodeList()
    tags = [tags[idx]]
  for child in tags:
   children.extend(getChildren(child, parts[1:]))
  return children
  
 def getter(self):
  children = getChildren(self,parts)
  if parts[-1] == 'text()':
   return "".join(children)
  m = RE_INDEXED.match(parts[-1])
  if m:
   try:
    string.atoi(m.group(2))
    return document.createHTMLElement(self,children[0]) if len(children) > 0 else None
   except ValueError: 
    pass
  return HTMLCollection(children)

 def setter(self, value):
  tag = self 
  for part in parts:
   if part == '':
    continue
   elif part == 'text()':
    if self.nodeName=="#text":
     t=Text(value)
     self.parent.replaceChild(n,self)
    else:
     self.appendChild(Text(value))
     return
   else:
    child = self.getElementsByTagName(part)
    if child.length==0:
     child=self.appendChild(Element(part))
     tag = child
  tag.appendChild(Text(value))
 return property(getter) if readonly else property(getter, setter)

class document(Node):
 #x='''
 @property
 def ownerDocument(self):
  return None
 @ownerDocument.setter
 def ownerDocument(self,x):
  self._ownerDocument=x
  self._ownerWindow=getattr(x,"window",None)
#'''

 @property
 def activeElement(self):
  return None

 @property
 def location(self):
  return self.window.location
 @location.setter
 def location(self,x):
  self.window.location.href=x

 def __getitem__(self,x):
  ret=self.getElementById(x)
  if ret:
   return ret
  if hasattr(self,x):
   return getattr(self,x)
  names=self.getElementsByName(x)
  if names:
   return names[0]

 @property
 def readyState(self):
  return self._readyState
 @readyState.setter
 def readyState(self,x):
  log("readyState:",x)
#  time.sleep(1)
  if x=="complete":
   log("events running")
   self.body.evt_domcontentloaded()
   self.body.evt_load()
#  self.body.evt_ready()
   self._readyState=x
   self.evt_readystatechange()

 def x__str__(self):
  return "".join([str(i) for i in self.childNodes])

 _type="document"
 _typeI=Node.DOCUMENT_NODE
 _TAGS = {
  "html":HTMLHtmlElement,
  "head":HTMLHeadElement,
  "link":HTMLLinkElement,
  "a":HTMLLinkElement,
  "title":HTMLTitleElement,
  "meta":HTMLMetaElement,
  "base":HTMLBaseElement,
  "isindex":HTMLIsIndexElement,
  "style":HTMLStyleElement,
  "body":HTMLBodyElement,
  "table":HTMLTableElement,
  "tr":HTMLTableRowElement,
  "tbody":HTMLTableSectionElement,
  "thead":HTMLTableSectionElement,
  "tfoot":HTMLTableSectionElement,
  "form":HTMLFormElement,
  "select":HTMLSelectElement,
  "optgroup":HTMLOptGroupElement,
  "option":HTMLOptionElement,
  "input":HTMLInputElement,
  "textarea":HTMLTextAreaElement,
  "button":HTMLButtonElement,
  "applet":HTMLAppletElement,
  "img":HTMLImageElement,
  "script":HTMLScriptElement,
  "frameset":HTMLFrameSetElement,
  "frame":HTMLFrameElement,
  "iframe":HTMLIFrameElement,
 }

 @property
 def documentElement(self):
  ret=[i for i in self.childNodes if i.tagName=="HTML"]
  return ret[0] if ret else None

 @property
 def anchors(self):
  return self.getElementsByTagName("a")
 @property
 def applets(self):
  return self.getElementsByTagName("applet")
 @property
 def body(self):
#  log("document.body")
  return self.getElementsByTagName("body",_return1=1)
 @property
 def cookie(self):
  if self._cookies and self.noModCookies==1: return self._cookies
  ret=self.backendNoAsync("js.cookies",url=self.URL)
  self._cookies=ret
  self.noModCookies=1
  return ret if ret else ''
 @cookie.setter
 def cookie(self,x):
  ret=self.backendNoAsync("js.cookie.set.single",url=self.URL,cookie=x)
  self.noModCookies=0
  return x if ret else ''
 @property
 def domain(self):
  return self.location.hostname if self._domain==None else self._domain
 @domain.setter
 def domain(self,x):
  self._domain=x
  return x
 @property
 def defaultView(self):
  return self.window
 @property
 def forms(self):
  return self.getElementsByTagName("form")
 @property
 def images(self):
  return self.getElementsByTagName("img")
 @property
 def lastModified(self):
  return self.backendNoAsync("js.headers","last-modified")
 @property
 def scripts(self):
  return self.getElementsByTagName("script")
 @property
 def links(self):
  return HTMLCollection([i for i in self.getElementsByTagName("a")+self.getElementsByTagName("area") if i.tagName=="AREA" or (i.tagName=="A" and i.getAttribute("href"))])
 @property
 def referer(self):
  return '' #self.backendNoAsync("js.headers","referer")
 @property
 def styleSheets(self):
  return HTMLCollection(self._findElements("link",{"rel":"stylesheet"})+self._findElements("style",None))
 @property
 def title(self):
  return self.getElementsByTagName("title")[0].textContent
#self._findElements("title",attrs=None,count=1).textContent
 @title.setter
 def title(self,t):
  title=self.getElementsByTagName("title")[0]
  while title.childNodes.length:
   title.removeChild(title.childNodes[0])
  title.appendChild(self.createTextNode(t))
  return self.title
 @property
 def URL(self):
  return self.url
 @property
 def url(self):
  return self.location.href
 @property
 def doctype(self):
  return DocumentType("html")
 @property
 def documentElement(self):
  return self.getElementsByTagName("html",_return1=1)
 @property
 def documentURI(self):
  return self.location.href
 @property
 def inputEncoding(self):
  return 'ISO-8859-1'
 @property
 def strictErrorChecking(self):
  return 0
 @property
 def xmlEncoding(self):
  return self.inputEncoding
 @property
 def xmlStandalone(self):
  return 0
 @property
 def xmlVersion(self):
  return "1.0"

 def __init__(self,_ownerDocument=None,window=None):
  self.all_nodes=[self]
  super(document,self).__init__("")
#  fh=open("./newDomLog.txt","ab"); fh.write(str(kw)); fh.close()
  self._readyState="loading"
  self._domain=None
#  _url=kw['url'] if "url" in kw else ""
  self._closed=0
  self.window=window
#  self.location=self.window.location
  self.ownerDocument=_ownerDocument
  self.implementation=DOMImplementation(_ownerDocument=self)
  self.domimpl=self.implementation
  self.noModCookies=0
  self._cookies=''

 def createEvent(self,eventType):
  return Event(eventType)

 def open(self):
  self.closed=0
 def close(self):
  self.closed=1

 def write(self,*l):
  for i in l:
   i='' if not i else i
   log("write:"+str(i))
   if self.readyState=="loading" or 1==1:
#    log("write:currentItem:",str(self.ownerWindow.document._current))
    parsed=self._parseToNodes(i)
    log("parseResult:",type(parsed))
    [self.ownerWindow.document._current.parentNode.appendChild(j) for j in parsed]
    return
    node=self
    while node.lastChild and node.lastChild.nodeType==Node.ELEMENT_NODE:
     node=node.lastChild
     log("write(loading):",repr(node))
    node.innerHTML=i
   else:
    log("write(not loading):",repr(self._current),i)
#    log("write:",j,type(j))
    for j in parseString(i,fragment=1):
     try:
      if hasattr(self,"_current"):
       index=None
       point=self._current
       log("write:point:",repr(point))
       if point.nextSibling!=None: index=point.parentNode.childNodes.index(point.nextSibling); point=point.nextSibling
       else: index=point.parentNode.childNodes.index(point)+1; point=point.parentNode
       point.insertBefore(j,index)
      else:
       self.body.appendChild(j)
     except Exception,e:
      log("writeErr:",e,j,type(j)); generate_error_report(e)
#     log("writeDone:",j,type(j))

 def writeln(self,*l):
  for i in l:
   self.write(i)
   self.write("\n")

 def _findElements(self,tag="",attrs={},_element=None,count=0):
  _element=_element if _element!=None else self 
  l=[]
  for i in _element.childNodes:
   if i.tagName==tag:
    if attrs==None:
     if count==1: return i
     l.append(i)
#     continue
    elif [k for k in attrs if k not in i.attrs]==[] and [k for k in attrs if (i.getAttribute("k").lower()==attrs[k].lower()) or attrs[k]==-1]:
     if count==1: return i
     l.append(i)
   l.extend(self._findElements(tag=tag,attrs=attrs,_element=i))
  return l

 def adoptNode(self,other):
  n=self.newNode(_type=other.nodeType,_parent=None,_value=other,_ownerDocument=self)
  return n

 def createAttribute(self,v):
  x=Attr(v)
  x.ownerDocument=self
  return x

 def createCDATASection(self,v):
  x=CDATASection(v)
  x.ownerDocument=self
  return x

 def createComment(self,v):
  x=Comment(v)
  x.ownerDocument=self
  return x

 def createDocumentFragment(self):
  x=DocumentFragment('')
  x.ownerDocument=self
  return x

 def createHTMLElement(self,v):
  x=HTMLElement(v)
  x.ownerDocument=self
  return x

 def createElementNS(self,uri,name):
  e=self.createElement(name)
#e=Element(name)
  e.namespaceURI=uri
  if ":" not in name: name=":"+name
  e.prefix,e.localName=name.rsplit(":",1)
  return e

 def createElement(self,v):
  if not isinstance(v,basestring): log("createElement weird:","|"+str(type(v))+str(v)+"|"); v=''
  if 1==1 or v.isalnum() and v[0].isalpha():
   log("creating element:",v)
   x=self._TAGS.get(v.lower(),HTMLElement)(v)
   x.ownerDocument=self
   return x

 def createTextNode(self,v):
  x=Text(v)
  x.ownerDocument=self
  return x

 def getElementById(self,id,_i=0,_element=None):
#  log("gebi",id)
  for i in self.iterNodes():
   if i._isElement and i.hasAttribute("id") and i.getAttribute("id")==id:
    return i
  return None

 def importNode(self,other):
  c=self.adoptNode(other)
  return c

 def renameNode(self,oldNode,namespace,newName):
  oldNode._nodeName=newName
  return oldNode

 def normalizeDocument(self):
  self.documentElement.normalize()

re_nums=re.compile("&#\d+;")
re_ent=re.compile("&\w+;")
html_special={
160:" ",
183:"*",
"apos":"'",
"nbsp":' ',
"lt":"<",
"gt":">",
8217:"'",
}

def stripWeird(s,unicodeOnly=1):
 try:
  s=s.decode('utf-8')
 except:
  pass
 lord=ord
 if not s: return s
 matches=[i for i in set(tuple(s)) if lord(i)>127]
 for i in matches:
  if i in html_special:
   s=s.replace(i,html_special[i])
  elif lord(i) in html_special:
   s=s.replace(i,html_special[lord(i)])
  else:
   s=s.replace(i,"")
 if unicodeOnly==1: return s
 matches = re_nums.findall(s)
 if matches:
  hits = set(matches)
  for hit in hits:
   name=int(hit[2:-1])
   if name >=0 and name<=127:
    s=s.replace(hit,chr(name))
   elif name in html_special:
    s=s.replace(hit,html_special[name])
   else:
    s=s.replace(hit,"")
 matches=re_ent.findall(s)
 if len(matches)>0:
  hits = set(matches)
  amp = "&"
  if amp in hits:
   hits.remove(amp)
  for hit in hits:
   name = hit[1:-1]
   if name in html_special:
    s=s.replace(hit,html_special[name])
   else:
    s=s.replace(hit,"")
  s = s.replace(amp, "&")
 return s

def getFullDom(url="",fc="",newJS=0,callback_q=None,pageQ=None,resolver=None):
 fc=stripWeird(fc,1)
 p=parserThread(fc=fc,url=url,callback_q=callback_q,pageQ=pageQ,resolver=resolver)
 p.start()
 log("running parser thread:",str(p))
 return p
