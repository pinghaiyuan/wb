import utils,sys,threading,Queue,os,os.path,time,binascii,urllib,urlparse
import requests,wbCookiejar
#if __name__=='__main__':
# sys.stderr=sys.stdout

class fileQuickCache(object):
 def __init__(self,directory=None,expires=86400):
#  print "cache.init"
  if directory==None: directory=utils.filename("cache")
  if not directory.endswith(os.sep): directory+='/'
  if not os.path.exists(directory): os.makedirs(directory)
  self.directory=directory

 def getKey(self,url,method="get"):
  return self.directory+str(binascii.crc32(method+"/"+url))

 def remove(self,url,method="get"):
  if url in self:
   os.remove(self.getKey(url,method))

 def writeCache(self,resp):
  url=resp.url
  if "//bmcginty.hopto.org" in url:
   return None
  l=[]
  l.append(str(resp.method))
  l.append(str(url))
  l.append(str(resp.status_code))
  for k,v in resp.headers.items():
   l.append(": ".join((str(k),str(v))))
  l.append('')
  l.append(str(resp.content))
  try:
   s="\n".join(l)
  except:
   return None
  h=self.getKey(url,resp.method)
  utils.log("cache:writing to "+h)
  fh=open(h,"wb")
  fh.write(s)
  fh.close()

 def readCache(self,url,method="get"):
  hash=self.getKey(url,method)
  try:
   fh=open(hash,"rb")
   fc=fh.read()
   fh.close()
  except:
   return None
  headers,body=fc.split("\n\n",1)
  method,url,code,headers=headers.split("\n",3)
  try:
   code=int(code)
  except:
   code=-2
  headers=dict([i.split(": ",1) for i in headers.split("\n")])
  x=quickResponse(method,url,code,headers,body)
  return x

 def __contains__(self,url,method="get"):
  ret=os.path.exists(self.getKey(url,method))
  utils.log("cache:checking cache for url %s, " % (url,)+(" exists " if ret else " not found "))
  return ret

class cookieRequest(object):
 shortcuts={"protocol":"scheme"}
 def __init__(self,url):
  self.url=url
  self.up=urlparse.urlparse(url)
 def __getattr__(self,x):
  return getattr(self.up,self.shortcuts.get(x,x))
 def get_full_url(self):
  return self.url
 def get_host(self):
  return self.hostname
 def get_type(self):
  return self.protocol
 def unverifiable(self):
  return 0
 def get_origin_req_host(self):
  return self.hostname
 def has_header(self):
  return 0
 def get_header(self,h,default=None):
  return default
 def header_items(self):
  return ()
 def add_unredirected_header(self,k,v):
  pass


class quickResponse(utils.box):
 def __init__(self,method,url,code,headers,body):
  self.method=method
  self.headers=headers
  self.content=body
  self.status_code=code
  self.url=url

 def __str__(self):
  return str(dict(headers=self.headers,url=self.url,status_code=self.status_code,content=self.content))
 def text(self):
  return self.content

 def iter_lines(self):
  for i in self.content.split("\n"):
   yield i

 def iter_content(self,chunk_size=1,decode=0):
  pos=chunk_size
  lc=len(self.content)
  while pos<lc:
   x=self.content[pos:pos+chunk_size]
   pos+=chunk_size
   yield x

 def cookies(self):
  return []

class protocol_about_class(object):
 def about_blank(self,url,count=0,**kw):
  return quickResponse("get","about:blank",200,{},"<html><head><title></title></head><body>about:blank</body></html>")

 def about_navauth(self,url=None,count=0,**kw):
  if "uri" not in kw or "method" not in kw or "username" not in kw or "password" not in kw or "authmethod" not in kw:
   return self.about_blank(url,count,**kw)
  data=kw['data']
  data['auth']=(kw['username'],kw['password'])
  data['nocache']=1
  data['url']=data['uri']
  utils.log("about_navauth",str(data))
  return getattr(self,"protocol_"+self.getProtocol(kw['uri']))(count=count,**data)
#url=kw['uri'],count=count,method=kw['method'],authmethod=kw['authmethod'],auth=(kw['username'],kw['password']))

class singleUrlFetcher(threading.Thread,protocol_about_class):
 def __init__(self,url="",q=None,quickCache=None,**kw):
  threading.Thread.__init__(self)
  self.daemon=1
#  print "singleUrlFetcher",url
  self.url=url
  self.q=q
  self.quickCache=quickCache
  self.kw=kw

 def protocol_file(self,url="",**kw):
  utils.log("launched file protocol")
  prot,url=url.split("//",1)
  utils.log("prot,url",prot,url)
  parts=url.split("/")
  utils.log("parts",parts)
  hostname="localhost"
  if parts[0] in ["localhost",""]: parts=parts[1:]
  path=os.path.realpath("/"+"/".join(parts))
  real_url="file://%s%s" % (hostname,path,)
  utils.log("got path",real_url)
  utils.log("file:found path",path)
  if os.path.isdir(path):
   code,content=self.generateDirectoryList(path)
  elif os.path.isfile(path):
   code,content=self.tryOpenLocalFile(path)
  else:
   code=404
   content='File "%s" not found.<br>Please check the path and filename, and try your request again.' % (path,)
  return utils.box(url=real_url,status_code=code,content=content,headers={})

 def tryOpenLocalFile(self,path):
  try:
   fh=open(path,"rb")
   fh.seek(0,2)
   fs=fh.tell()
   nfs=float(fs)/1024.0/1024.0
   if nfs>5.0:
    return 500,'File %s is too large to open.<br>Size:%.2f MB' % (path,nfs,)
   fh.seek(0,0)
   return 200,fh.read()
  except Exception,e:
   return 500,'Error occured when reading "%s"<br>Error:%s' % (path,str(e),)

 def generateDirectoryList(self,path,hostname="",uri=""):
  files=[os.path.join(path,i) for i in os.listdir(path)]
  files.sort()
  html="""<html>
<head>
<title>Index of file://%s/%s/</title>
</head>
<body>
<h1>Index of file://%s/%s</h1>
<ul>
""" % (hostname,path,hostname,path,)
  for i in files:
   if os.path.isdir(i): html+='<li><a href="file://%s/%s">%s</a></li><br>' % (hostname,i,i.rsplit("/",1)[-1]+"/",)
   if os.path.isfile(i): html+='<li><a href="file://%s/%s">%s</a></li><br>' % (hostname,i,i.rsplit("/",1)[-1],)
  html+="""
</ul>
</body>
</html>
"""
  return 200,html

 def protocol_data(self,url,**kw):
  param,extra=url.split(":",1)
  cType,extra=extra.split(",",1)
  extra=urllib.unquote(extra)
  if ";" in cType:
   bits=cType.split(";")
   cType=bits[-1]
   if cType=="base64":
    extra=binascii.a2b_base64(extra)
  return utils.box(url=url,status_code=200,headers={},content=extra)

 def protocol_ftp(self,url,**kw):
  raise Exception('unsupported protocol')

 def protocol_https(self,url,count=0,**kw):
  kw['protocol']='https'
  kw['verify']=kw.get("verify",True)
  if kw['verify']==None: kw['verify']=False
  return self.protocol_http(url=url,**kw)

 def protocol_about(self,url=None,count=0,**kw):
  if hasattr(kw.get("data",None),"items"):
   for k,v in kw['data'].items():
    kw[k]=v
  if hasattr(kw.get("params",None),"items"):
   for k,v in kw['params'].items():
    kw[k]=v
  return getattr(self,"about_"+url.split(":",1)[-1].split("?",1)[0],self.about_blank)(url,count,**kw)

 def protocol_http(self,url=None,count=0,**kw):
  default_headers={"user-agent":"Mozilla/5.0 (Linux 3.2; rv:15.0) Gecko/20100101 Firefox/15.0.1; wb/1.0"}
  cookies=kw.get("cookies",None)
  method=kw.get("method","get").lower()
  headers=kw.get("headers",default_headers)
  trueFetch=1
  if url in self.quickCache and "nocache" not in kw and method!="post":
   try:
    ret=self.quickCache.readCache(url)
    trueFetch=0
   except Exception, e:
    utils.log("error fetching from cache",e)
  if trueFetch==1:
   utils.log("raw fetching "+url+", method="+method)
#elif method=="post" or ret==None:
   rSession=kw.get("session",requests)
   try:
    verify=kw.get("verify",True)
    params=kw.get("params",None)
    data=kw.get("data",None)
    auth=kw.get("auth",None)
    if auth and kw.get("authmethod",None):
     authmethod=kw.get("authmethod",None)
     if authmethod=="digest":
      auth=requests.auth.HTTPDigestAuth(auth[0],auth[1])
    ret=getattr(rSession,method)(url=url,headers=headers,cookies=cookies,prefetch=1,allow_redirects=0,params=params,data=data,verify=verify,auth=auth,proxies={})
    ret=quickResponse(ret.request.method.lower(),ret.url,ret.status_code,ret.headers,ret.content)
#    utils.log("rawFetch:got:",ret)
    if int(ret.status_code)<300 and method!="post" and "nocache" not in kw: self.quickCache.writeCache(ret)
   except Exception,e:
#    utils.log("error raw fetching",url,"error:",e)
    return utils.box(url=url,error=e,status_code=-1,content="",headers={})
  if 300<=int(ret.status_code)<400 and "location" in ret.headers:
   loc=ret.headers['location']
   if "://" not in loc:
    loc=utils.makeFullLink(loc,url)
   kw['method']="get"
#   kw['nocache']=1
   ret=getattr(self,"protocol_"+self.getProtocol(loc))(url=loc,count=count+1,**kw)
   ret.url=loc
  if ret.status_code in (401,403):
#os.path.exists("templates/%d" % (ret.status_code,)):
   ret=quickResponse(ret.method,"about:navauth",200,{},utils.makeTemplate(str(ret.status_code),[{"o_resp_headers":ret.headers},kw,ret,locals()]))
  return ret

 def getProtocol(self,url):
  return url.split(":",1)[0]

 def run(self):
  result=getattr(self,"protocol_"+self.getProtocol(self.url))(url=self.url,**self.kw)
#  utils.log("got result:",result,result.status_code,self.q,"\n")
  self.q.put(result)
  return

class urlFetcherClass(threading.Thread):
 def __init__(self,cookies=None,urlFetcherClassQ=None,cache=None):
  threading.Thread.__init__(self)
  self.urlFetcherClassQ=urlFetcherClassQ
  self.internal_queues={}
#Queue.Queue()
  self.threads={}
  self.count=0
  self.urls={}
  self.cookies=wbCookiejar.sqliteCookiejar()
  self.s=requests.Session(cookies=self.cookies)
  self.quickCache=fileQuickCache(utils.filename("cache")) if cache==None else cache

 def addUrl(self,url="",key="",**kw):
  utils.log("addUrl:",url,"key",key)
  if key not in self.internal_queues: self.internal_queues[key]=Queue.Queue()
##  print "addUrl",url
  t=singleUrlFetcher(url=url,q=self.internal_queues[key],cookies=self.cookies,quickCache=self.quickCache)
#  print "added thread"
#  time.sleep(0.5)
  t.start()
  if key not in self.threads: self.threads[key]=[]
  self.threads[key].append(t)
#  print len(self.threads)
  self.count+=1
#  self.urls[key].append(url)

 def getSingleUrl(self,url="",download=1,**kw):
#  print "kw:",kw
#  print "getSingleUrl",url
#  if url in self.urls and download=0:
   #pull from q, re-insert at end, until url result found
#start a new thread just for this url, wait for url or timeout, return result or (url,None)
  q=Queue.Queue()
  oq=kw['q']
  del kw['q']
#  [kw.__del__(k) for k in ["cookies","url","quickCache","q"] if k in kw]
#  print kw
  t=singleUrlFetcher(url=url,cookies=self.cookies,quickCache=self.quickCache,q=q,**kw)
  st=time.time()
  et=time.time()+30
  t.start()
  while time.time()<et:
   try:
    res=q.get(block=0)
#    utils.log("got res from single-url-fetching thread",res)
    kw['q']=oq
    return res
   except Queue.Empty:
    pass

 def exit(self,**kw):
#  print "exit thread"
  sys.exit()

 def run(self):
  while 1:
   time.sleep(0.01)
   try:
    d=self.urlFetcherClassQ.get(block=0)
#    time.sleep(0.5)
    if d.get("name","null")=="exit": return 0
    target=getattr(self,d.get('name','getSingleUrl'))
#    utils.log("requesting:",d,"with function",target)
    ret=target(**d)
#    utils.log("got",ret,"with",d,"and function",target)
#    utils.log("ret:",ret,"q:",d.get("q"),"target",target)
    if "q" in d and d['q']!=-1:
     if "q" in d:
      if target!=self.getAll:
       try:
        ret.name='data'
       except:
        pass
#      utils.log("putting ",ret,"into",d['q'])
      d['q'].put(ret)
     else:
      log("result discarded",str(target)) #ret
   except Queue.Empty:
     pass

 def setCookie(self,key="",url="",cookie="",**kw):
  params=cookie.split(";")
  cookie,params=params[0],params[1:]
  params=[i.strip() for i in params if "=" in i]
  params=dict([i.split("=",1) for i in params])
  if "=" not in cookie: cookie+="="
  n,v=cookie.split("=",1)
  up=urlparse.urlparse(url)
  params['domain']=params.get("domain",up[1])
  params['path']=params.get("path",up[2])
  params['expires']=params.get("expires",None)
  if params['expires']==None and params.get("max-age",None):
   m=params['max-age']
   try:
    m=int(m)
    m=time.time()+m
    params['expires']=m
   except:
    pass
   params.pop("max-age")
  elif params['expires']!=None:
   formats=['%a, %d %b %Y %H:%M:%S %Z','%a, %d-%b-%Y %H:%M:%S %Z']
   trytimefix=None
   for format in formats:
    try:
     params['expires']=time.mktime(time.strptime(params['expires'],format))
     trytimefix=1
     break
    except:
     continue
   if trytimefix==None:
    params['expires']=None
  params['name']=n
  params['value']=v
  params['httpOnly']=params.get("httpOnly",0)
  params['secure']=params.get("secure",0)
  params['discard']=params.get("discard",1)
  x=utils.box(**params)
  if self.cookies.set_cookie(x):
   return cookie
  else:
   return None

 def getCookies(self,key="",url="",**kw):
  ret=self.cookies._cookies_for_request(cookieRequest(url.replace(":80","/",1) if (":80/" in url or url.endswith(":80")) else url))
  utils.log("urlFetcher:got cookies for url:%s:%s" % (str(url),str(ret),))
  return ret

 def getAll(self,key="",**kw):
  l=[]
  if key not in self.internal_queues or key not in self.threads:
   utils.log("key not found:",key)
   return l
  tmpCounter=0
  checkCounter=0
#  print "threads:",len(self.threads)
  while tmpCounter<len(self.threads[key]):
#self.internal_q.qsize() or (tmpCounter<self.count or checkCounter<100):
   try:
    l.append(self.internal_queues[key].get())
    tmpCounter+=1
   except:
    pass
#  print l
  return l

urlFetcherQ=Queue.Queue()
urlFetcher=urlFetcherClass(urlFetcherClassQ=urlFetcherQ)
urlFetcher.start()

def addUrl(url,**kw):
 urlFetcherQ.put(dict(name='addUrl',url=url,q=-1,**kw))
# print "global addUrl, added url",url
# print urlFetcherQ.get()

def setCookie(url="",cookie=""):
 q=Queue.Queue()
 urlFetcherQ.put(dict(name="setCookie",url=url,q=q,cookie=cookie))
 return q.get()

def getCookiesForUrl(url=""):
 q=Queue.Queue()
 urlFetcherQ.put(dict(name="getCookies",url=url,q=q))
 return q.get()

def getAllUrls(key=""):
 q=Queue.Queue()
 urlFetcherQ.put(dict(name='getAll',q=q,key=key))
 return q.get()

def getSingleUrl(url,**kw):
 q=Queue.Queue()
 urlFetcherQ.put(dict(name='getSingleUrl',q=q,url=url,**kw))
 try:
  ret=q.get(block=30)
 except Exception,e:
#  print e
  ret=quickResponse("get",url,0,[],"")
 return ret

def getSingleUrlContents(url,**kw):
 ret=getSingleUrl(url,**kw)
 return ret.content

if __name__=='__main__':
 # import sys
 # sys.stderr=sys.stdout
 fc=getSingleUrlContents("http://www.mpsaz.org/",nocache=1)
 print fc[:200]
# print len(fc)
 for i in xrange(20,40):
  if i%2==0: key="even"
  else: key="odd"
  addUrl("http://www.linux-speakup.org/cgi-bin/echo.cgi?number=%d" % (i,),key=key)
 # time.sleep(0.2)
 #time.sleep(10)
 x=getAllUrls(key="even")
 y=getAllUrls(key="odd")
 print len(x),len(y)
 #print "got all urls"
 #print len(x)
 urlFetcherQ.put(dict(name="exit"))
 #print "exitting main thread"
 utils.log(dict(system=1,quit=1))
# time.sleep(0.1)
 t=2
 while t>1:
  t=len(threading.enumerate())
  print t
  time.sleep(0.01)
 sys.exit()
