#!/bin/bash
if [[ $UID != '0' ]]
then
echo "We did not detect having root capability on this system.
Please run sudo ./build_depts.sh to continue this installation.
Please review the source of this script before running it as root, to insure that it will not harm your system."
exit 0;
fi
base="`pwd`"
dest="$base/build"
echo "You require gcc, g++, python 2.5 or later, an svn command-line client, libboost-python and scons to build this package.
Please exit and install these prerequisits, if they are not already installed.
Your librarys will be built in 
\"$dest/\""
if [ -x "$dest" ]
then
echo "We are now deleting 
\"$dest/\" 
and all of its subdirectories. You have five seconds to cancel this deletion (Press control c to cancel).
Do nothing to continue the deletion, and create a clean build environment."
sleep 5
echo "deleting."
#remove the librarys themselves, leaving the existing .installed files (librarys are large, .installed files are just empyt files)
find "$dest" -mindepth 1 -maxdepth 1 -type d -exec touch "{}.installed" \; -exec rm -Rf {} \;
fi
echo "Refreshing contents of $dest"
if [ ! -x "$dest" ]
then
mkdir "$dest/"
fi
cd "$base/"
python build_pyv8_test.py
code=$?
if [[ $code == 127 ]]
then
echo "You must have at least python 2.5 installed.
Please install python, from http://www.python.org/, and rerun this installer."
exit 0
fi
cd "$dest/"
if [[ $code != 0 ]]
then
if [ ! -f "$dest/v8.installed" ]
then
echo 'Grabbing V8 code.'
svn checkout http://v8.googlecode.com/svn/trunk/ v8
touch v8.installed
cd "v8/"
echo 'downloading gyp for building V8.'
svn co http://gyp.googlecode.com/svn/trunk build/gyp
make native
fi
if [ ! -f "$dest/pyv8.installed" ]
then
cd "$dest"
svn checkout http://pyv8.googlecode.com/svn/trunk/ pyv8
touch "pyv8.installed"
cd "pyv8/"
#placeholder for future patches, if needed
export V8_HOME="$dest/v8/"
python setup.py build
python setup.py install
fi
else
touch "$dest/v8.installed"
touch "$dest/pyv8.installed"
fi
cd "$dest/"
if [ ! -f "buildsystem.installed" ]
then
git clone "git://git.netsurf-browser.org/buildsystem.git"
fi
if [ ! -f "libhubbub.installed" ]
then
git clone "git://git.netsurf-browser.org/libhubbub.git"
fi
if [ ! -f "libparserutils.installed" ]
then
git clone "git://git.netsurf-browser.org/libparserutils.git"
fi
for i in buildsystem libparserutils libhubbub
do 
if [ ! -f "$i.installed" ]
then
cd $i
make install COMPONENT_TYPE=lib-shared PREFIX=/usr
cd ..
touch "$i.installed"
fi
done
echo "building parser module."
cd "$base/modules/hubbub/"
python setup.py build
find ./ -type f -iname '*_hubbub_parser.so' -exec cp {} ../ \;
rm -Rf ./build/
cd ../..
