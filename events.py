#events generator
import PyV8,time
import utils
from utils import log
log("logging from events")
"""hackish construction is due to the sheer number of events in the HTML standards.
Making a change in this file, without this format, would require upwards of 30 extra changes.
Until these functions have been finalized, please maintain this construction if at all possible.
"""
class events(object):
###events###
 events={}
 event_names=\
"onabort onactivate onafterprint onafterupdate onbeforeactivate onbeforecopy onbeforecut onbeforedeactivate onbeforeeditfocus onbeforepaste \
onbeforeprint onbeforeunload onbeforeupdate onblur onbounce oncellchange onchange onclick oncontextmenu oncontrolselect \
oncopy oncut ondataavailable ondatasetchanged ondatasetcomplete ondblclick ondeactivate ondomcontentloaded ondrag ondragend ondragenter \
ondragleave ondragover ondragstart ondrop onerror onerrorupdate onfilterchange onfinish onfocus onfocusin \
onfocusout onhashchange onhelp oninput onkeydown onkeypress onkeyup onload onlosecapture onmessage \
onmousedown onmouseenter onmouseleave onmousemove onmouseout onmouseover onmouseup onmousewheel onmove onmoveend \
onmovestart onoffline ononline onpaste onpropertychange onready onreadystatechange onreset onresize onresizeend onresizestart \
onrowenter onrowexit onrowsdelete onrowsinserted onscroll onsearch onselect onselectionchange onselectstart onstart \
onstop onsubmit onunload"
 event_names=set(event_names.split(" "))
 for e in event_names:
  shortEventName=eNameShort=e[2:]
  eventName=eName=e
  exec """@property
def %(eventName)s(self):
 evs=self.events.get("%(shortEventName)s",[])
# log("evs1:",repr(evs))
 if evs:
  evs=[i for i in evs if i.fromAttribute==1]
#  log("evs1:",repr(evs))
 if evs:
#  log("evs3:",repr(evs))
  return evs[0].original
 return None
@%(eventName)s.setter
def %(eventName)s(self,x,captures=0):
 evs=self.events.get("%(shortEventName)s",[])
 evs=[i for i in evs if i.fromAttribute==1]
 [self.events["%(shortEventName)s"].remove(i) for i in evs]
 if not x: return
 log('adding event'+str(repr(self))+', '+str(x))
 self.addEventListener("%(shortEventName)s",x,captures,fromAttribute=1)

def evt_%(shortEventName)s(self,evt=None):
 label='%(shortEventName)s'
# log("eventCalled:",label)
 if not evt:
  evt=Event("%(shortEventName)s")
 if not evt._init:
  evt.initEvent()
 evt._target=self
 path=self.parentNodesL
# log("path:",repr(path))
 if path and getattr(path[-1],"nodeName",None)=="#document":
  path.append(self.ownerWindow)
#reverse for capturing phase
 path.reverse()
 log("event:%(shortEventName)s",str(path))
 lenPath=len(path)-1
 start=0
 end=lenPath
 evt._eventPhase=1
 i=0
 direction=1
 while 0<=i<=end:
  log("evt:i:"+str(i))
  if evt.propagationStopped:
   evt.unInit()
   return (-1*evt.defaultPrevented)+1
  if direction==0 and evt.eventPhase!=2:
   evt._eventPhase=2
  if direction==-1 and evt.eventPhase==2:
   evt._eventPhase=3
  elem=path[i]
  evt._currentTarget=elem
  listeners=elem.events.get("%(shortEventName)s",[])
  log("evt:element:"+repr(elem)+":"+str(len(listeners)))
  for listener in listeners:
   if evt.immediatePropagationStopped:
    evt.unInit()
    return (-1*evt.defaultPrevented)+1
   x=None
   log("captures:",listener.captures,"phase:",evt.eventPhase)
   if listener.captures and evt.eventPhase==1:
    x=listener.run(evt)
   elif not listener.captures and evt.eventPhase==3:
    x=listener.run(evt)
   if evt.eventPhase==2:
    x=listener.run(evt)
   if x==0:
    evt.preventDefault()
  if direction==0:
   direction=-1
   continue
  elif i==end and direction==1:
   direction=0
   continue
  i+=direction
  continue
 evt.unInit()
 return (-1*evt.defaultPrevented)+1
""" % {"shortEventName":e[2:],"eventName":e}

class Event(PyV8.JSClass):
 altKey,ctrlKey,metaKey,shiftKey=0,0,0,0
 bubbles=0
 button=-1 #0,1,2 for left,center,right
 cancelable=1
 charCode=0
 clientX,clientY=0,0
 _currentTarget=None #points to the current element whose listeners are being run
 _eventPhase=0 #capturing=1,at_target=2,bubbling=3
 _init=0
 layerX,layerY=0,0
 pageX,pageY=0,0
 relatedTarget=None #holds newly moved-to element for onmouseover event, or the element that was left for an onmouseout event
 screenX,screenY=0,0
 _target=None #event where action truly occurred (you click on a link, and a div parent of said link has this event-handler attached, that link would be assigned to the target property
 timeStamp=0
 type="NULL"
 which=charCode
 _preventDefault=0
 _stopPropagation=0
 _stopImmediatePropagation=0

 @property
 def currentTarget(self):
  return self._currentTarget

 @property
 def target(self):
  return self._target

 @property
 def propagationStopped(self):
  return self._stopPropagation

 @property
 def immediatePropagationStopped(self):
  return self._stopImmediatePropagation

 @property
 def eventPhase(self):
  return self._eventPhase

 @property
 def defaultPrevented(self):
  return self._preventDefault

 def unInit(self):
  self._init=0
  self._currentTarget=None
  self._eventPhase=0
  self.stopPropagation()

 def stopImmediatePropagation(self):
  self.stopPropagation()
  self._stopImmediatePropagation=1

 def stopPropagation(self):
  self._stopPropagation=1

 def preventDefault(self):
  self._preventDefault=1

 def initEvent(self,type="null",bubbles=1,cancelable=1):
  self.type=type
  self.bubbles=bubbles
  self.cancelable=cancelable
  self._preventDefault=0
  self._stopPropagation=0
  self._stopImmediatePropagation=0
  self._init=1

 def __init__(self,name):
  self.type=name
  self.timeStamp=time.time()
  self._eventPhase=0

